% !TEX encoding = utf8
% !TEX program = pdflatex
% !TEX root = ../not-the-end
% !TEX spellcheck = it-IT

\chapter{Creare le \glo{ambientazioni}}
\label{chap:creare-ambientazioni}
Ogni \glo{storia} ha bisogno di un'\glo{ambientazione}: un mondo in cui gli \glo{eroi} possano muoversi e con cui possano interagire.
La natura, l'ampiezza e il livello di dettaglio di un'\glo{ambientazione} dipendono da molti aspetti e possono variare moltissimo.

In alcuni casi l'\glo{ambientazione} sarà un'idea esclusiva del \glo{narratore}, che vuole riservare ai \glo{giocatori} una \glo{storia} ricca di sorprese e un mondo sconosciuto da esplorare.

Altre volte sarà uno sforzo collaborativo, in cui l'intero gruppo di gioco contribuirà a elencarne le caratteristiche, arricchendola di dettagli, luoghi, organizzazioni e problemi da affrontare.

Oppure l'\glo{ambientazione} potrebbe essere un universo narrativo già esistente, adattato a seconda delle necessità dei \glo{giocatori} e della \glo{storia}.

In questo \namecref{chap:creare-ambientazioni} trovi suggerimenti utili a porre le basi di un'\glo{ambientazione} e nella \namecref{part:esempi-ambientazione} \nameref{part:esempi-ambientazione} cinque \glo{ambientazioni} di esempio.

Per creare un'\glo{ambientazione} puoi seguire questi passaggi.

\begin{enumerate}
    \item \emph{Descrivi l'ambientazione in una frase.}
        Descrivi in una frase le caratteristiche principali dell'\glo{ambientazione}, ti servirà come riferimento durante i passi successivi.
    \item \emph{Metti a fuoco i dettagli necessari.}
        Focalizza quali sono i dettagli che vuoi approfondire e concentrati su quelli.
        Tieni in considerazione non solo l'\glo{ambientazione}, ma anche l'uso che intendi farne.
        La durata della \glo{storia}, la cadenza delle \glo{sessioni}, la solidità del gruppo di gioco, la voglia e il tempo che vuoi e puoi permetterti di dedicare al lavoro di dettaglio.
    \item \emph{Crea esempi di tratti, risorse, nomi e sfide.}
        Se vuoi rendere l'\glo{ambientazione} fruibile ad altre persone, creare degli elenchi esemplificativi di \glo{tratti}, \glo{risorse} e \glo{nomi} può essere un utile riferimento per chi la utilizzerà.
        Non cercare di coprire tutte le possibilità offerte dall'\glo{ambientazione}, concentrati sui suoi elementi cardine.
    \item \emph{Crea alcune sfide e alcuni eroi iconici.}
        \glo{Eroi} e \glo{sfide} sono due lati della stessa medaglia.
        Crearne alcuni esempi renderà l'\glo{ambientazione} più definita nella tua mente e più accessibile per chiunque ne voglia fare uso.
        Usa la stesura di \glo{eroi} e \glo{sfide} come un pretesto per familiarizzare con i lati dell'\glo{ambientazione} più a contatto con la \glo{storia}.
\end{enumerate}

\section{Descrivi l'\glo{ambientazione} in una frase}
In
\marginpar{Un paesino del Texas durante la Guerra di Secessione.}
questa fase, cerca di focalizzare gli elementi necessari a descrivere l'\glo{ambientazione} in poche parole.
Fissa alcune nozioni di base, come un tema portante, un quando o un dove.

Che
\marginpar{Una città volante stracolma di avventurieri, capace di viaggiare nel tempo.}
tu stia creando l'\glo{ambientazione} da solo o assieme al resto del tavolo da gioco, in questa fase il tuo scopo è ottenere una vista dall'alto che informi i \glo{giocatori} sul mondo da cui provengono i loro \glo{eroi} e il tipo di \glo{storie} che vivranno.

Nelle
\marginpar{L'interno della mente di una persona in coma, in cui tutto è possibile e sogni e incubi sono realtà.}
fasi successive potrai lasciarti ispirare dagli elementi chiave che avrai definito, ampliandoli e arricchendoli di dettagli.

\subsection{Trova l'idea generale dell'ambientazione}
A
\marginpar{Film noir, Italia durante la 2\textsuperscript{a} Guerra Mondiale, vampiri contro licantropi, \emph{Guerre Stellari}, post-apocalittico, divinità in declino che vivono nel mondo reale.}
volte, per scegliere un'\glo{ambientazione}, la cosa migliore è partire da ciò che conosci, da ciò che ti incuriosisce o da ciò che ti appassiona.
Un universo narrativo famoso, un genere della fiction, una tematica da esplorare, un luogo o un periodo storico del mondo reale o una sua versione alternativa.
Individua un tema a cui ispirarti e che ti aiuti a definire i temi portanti dell'\glo{ambientazione}.

Poniti domande che stimolino la tua fantasia e che ti aiutino a scegliere una strada da seguire.
Parti dagli interrogativi più semplici, in questa fase nessuna domanda è troppo banale.
\begin{itemize}
    \item Che tipo di \glo{storia} vuoi raccontare?
    \item A quale genere della fiction si rifà l'\glo{ambientazione}?
    \item In quale mondo o universo narrativo è \glo{ambientata} la \glo{storia}?
    \item Quali sono gli elementi fondamentali dell'\glo{ambientazione}?
    \item Quali sono gli elementi fuori tono o che escluderesti dall'\glo{ambientazione}?
    \item Quale opera di fantasia può fornire buoni spunti per definire l'\glo{ambientazione}?
    \item Quale periodo storico può fornire buoni spunti per definire l'\glo{ambientazione}?
    \item Quali esempi faresti a qualcuno a cui la vuoi spiegare in breve?
\end{itemize}

\subsection{Scegli un ``dove''}
Una
\marginpar{In un piccolo e freddo avamposto ai confini dell'impero, nel dedalo di prigioni della Fortezza della Veglia, in un piccolo pianeta forgia ai margini del bordo esterno, una stanza da cui gli \glo{eroi} non possono uscire.}
volta descritta l'\glo{ambientazione} in una frase, restringi il campo e scegli un luogo che accolga la \glo{storia}.
A prescindere da quanto sia vasta l'ambientazione, la \glo{storia} ha bisogno di un contesto in cui cominciare.

\begin{itemize}
    \item In che luogo comincia la \glo{storia}?
    \item Qual è la scala dell'\glo{ambientazione}?
        Quali distanze copriranno gli \glo{eroi}?
    \item Quali sono i luoghi, i centri di potere e le organizzazioni più importanti?
    \item Quali sono le caratteristiche geografiche e geopolitiche di spicco?
\end{itemize}

\subsection{Scegli un ``quando''}
Che
\marginpar{Appena prima della caduta del Muro di Berlino, nei mitici Anni '80, in un lontano e indefinito futuro post-atomico. subito dopo la scoperta dell'energia elettrica, al termina di una Seconda Guerra Mondiale alternativa da cui la Germania è uscita vittoriosa, la notte che precede l'invasione degli eserciti degli Elfi Oscuri.}
si tratti di un periodo storico, di un momento preciso nel calendario dell'universo narrativo o di un concetto vago e astratto, il tempo influenza il nostro modo di vedere e vivere l'\glo{ambientazione}.

\begin{itemize}
    \item In che epoca o periodo storico è ambientata la \glo{storia}?
    \item Quali conoscenze e che livello di progresso sono comuni nell'\glo{ambientazione}?
    \item La \glo{storia} comincia in prossimità di avvenimento storici importanti?
        Quali?
    \item Ci sono elementi anacronistici nell'\glo{ambientazione}?
        Quali?
\end{itemize}

\subsection{Dai un tocco personale}
Se hai preso ispirazione da un'\glo{ambientazione} già esistente o dal mondo reale potresti decidere di cambiare qualche dettaglio e renderla più personale.
Chiediti cosa potrebbe esserci di diverso, se ci sono elementi che desideri cambiare, che aggiungeresti o che preferisci eliminare.
Un solo elemento inaspettato potrebbe cambiare radicalmente il sapore o il fulcro di un'\glo{ambientazione}.

\section{Metti a fuoco i dettagli necessari}
Ogni \glo{storia} è diversa e si sviluppa a modo suo, ogni gruppo di gioco ha abitudini
e necessità differenti.
Alcune \glo{storie} durano a lungo, altre sono molto brevi.
Alcune \glo{storie} si svolgono in poche location molto dettagliate, altre invece sono caratterizzate da lunghi viaggi e tanti contesti diversi.

Modalità e livello di definizione dell'\glo{ambientazione} dovrebbero adattarsi ai
fini della \glo{storia}.
Chiediti quali aspetti vale la pena approfondire e quali è meglio
lasciare indefiniti.

Per evitare sforzi inutili è meglio adottare un approccio pratico: parti da
quello che già sai e muoviti di conseguenza, investendo energie solamente in
ciò che ha valore.

\subsection{L'ambientazione esiste già}
Non
\marginpar{Emanuele e il suo gruppo voglio giocare nell'universo di \emph{Guerre Stellari}.
    Avendo poco tempo a disposizione e non potendo essere sempre tutti presenti alle \glo{sessioni}, il gruppo decide di giocare una serie di brevi avventure concatenate, in cui differenti gruppi di \glo{eroi} facenti parte dell'Alleanza Ribelle viaggiano di pianeta in pianeta combattento gli emissari dell'Impero e le loro organizzazioni locali.

    L'opprimente presenza dell'Impero deve sentirsi, quindi la \glo{storia} viene \glo{ambientata} tra \emph{Episodio V} e \emph{Episodio VI}, quando l'Impero è in piena ascesa e i protagonisti della trilogia originale si stanno riprendendo dalla sconfitta.}
c'è nulla di male nello scegliere di giocare in un'\glo{ambientazione} già esistente, soprattutto se la conoscete bene.
Appoggiarsi su \glo{ambientazioni} già note abbasserà drasticamente il livello di preparazione encessario.
Potrete contare su materiali già pronti e su una base di nozioni condivise.

Se all'interno del gruppo c'è una grande disparità di esperienza sull'\glo{ambientazione} è utile condividere materiali che permettano a tutti di partire con le nozioni minime per poter giocare.
Non è necessario che tutti conoscano a memoria ogni particolare dell'\glo{ambientazione}, l'esperienza arriverà giocando.

Se state utilizzando un'\glo{ambientazione} famosa la cosa migliore è decidere assieme un punto di partenza, che permetta ai \glo{giocatori} di inserire efficacemente i loro \glo{eroi} nel contesto e al \glo{narratore} di dare il via alla \glo{storia} senza sforzo.

Chiedetevi quali tra le tematiche portanti dell'\glo{ambientazione} esplorare e quali invece trascurare.
Scegliete se volete essere al centro dell'\glo{ambientazione} o esplorarne i margini, lasciandola di sottofondo alla \glo{storia} vissuta dagli \glo{eroi}.

\subsection{Conosci la cadenza delle sessioni o la durata della storia}
Se
\marginpar{Roberto e il suo gruppo vogliono giocare una singola \glo{sessione} in cui gli \glo{eroi} cercano di sopravvivere a una minaccia molto più forte di loro.
    Dopo una breve discussione si opta per un sapore fantascientifico.
    L'ambientazione non è nulla più della mappa di un'astronave in cui la \glo{sessione} verrà improvvisata.}
la \glo{storia} che giocherete ha una durata o una cadenza prestabilita è facile farsi un'idea del numero di situazioni e luoghi che sarà possibile esplorare e del livello di complessità oltre il quale non spingersi.
Se il tempo che dedicherete alla \glo{storia} è limitato evitate di sviluppare l'\glo{ambientazione} troppo in profondità, concentratevi su pochi elementi prediligendo quelli che vi interessano maggiormente.

\nte{} permette al \glo{narratore} di non dover preparare le singole situazioni di gioco.
Se volete approfondire degli elementi, prediligete quelli che vi permettano di vivere al meglio l'\glo{ambientazione}: la cultura, i modi di fare, i nomi più comuni.
Il \glo{narratore} sarà libero di approfondire l'\glo{ambientazione} a suo piacimento, sulla base delle sue preferenze e degli obiettivi narrativi che vuole ottenere, nel rispetto dello spirito di collaborazione con il tavolo.

\subsection{Sei un cultore dell'ambientazione}
A
\marginpar{Claudio ha sempre voluto narrare una \glo{storia} \glo{ambientata} nel Giappone Feudale.
    È un appassionato di cultura giapponese: ha visto film, letto una montagna di libri, ed è stato più volte in Giappone.
    Nel gruppo di gioco alcuni conoscono il Giappone per sommi capi e altri hanno giocato a videogiochi ambientati in un generico Oriente.
    Claudio decide di mostrare un film di Akira Kurosawa per chiarire a tutti cos'ha in mente, dopodiché spende una settimana a selezionare libri e film ai quali ispirarsi per \glo{ambientare} la \glo{storia} e per aiutare i \glo{giocatori} a creare gli \glo{eroi}.}
volta la passione di una persona al tavolo per un'\glo{ambientazione} è talmente grande da coinvolgere il resto del gruppo di gioco.
Spesso questa persona si farà carico di \glo{narrare} la \glo{storia}.
Questa è una splendida occasione per rendere accessibile la propria passione ad altre persone e il modo migliore di farlo è concentrarsi sul fornire pochi elementi alla volta, facendoli assaporare uno per uno.

In questi casi la fatica nel dettagliare l'\glo{ambientazione} non si sentirà, e ci si dovrà sforzare per limitare la propria creatività per non mettere troppa carne al fuoco.
È molto facile sovrastimare la quantità di materiale necessario ai \glo{giocatori}  per godersi la \glo{storia}, con il rischio di affollarla di elementi superflui.

Questa inoltre è un'occasione perfetta per esplorare la libertà concessa dal sistema di gioco.
Tentare di preparare il minimo e rispondere agli stimoli dei \glo{giocatori} è più facile in un contesto che il \glo{narratore} conosce molto bene.

\subsection{Stai creando l'ambientazione da zero}
Se
\marginpar{Gianni e i suoi \glo{giocatori} vogliono creare un'\glo{ambientazione} fantascientifica, insaporita da un tocco di misticismo e superstizione.

    Il gruppo di gioco disegna una mappa che comprende cinque sistemi stellari, ognuno caratterizzato da un elemento chiave.
    \begin{itemize}
        \item \emph{Rea-Ko}: il pianeta di origine dei Korr, una specie bellicosa che da poco tempo è entrata a far parte dell'Unione Galattica.
        \item \emph{Sigma-13}: mondo forgia da cui provengono gli armamenti di metà della Galassia.
        \item \emph{La Singolarità}: uno squarcio che sta lentamente fagocitando un sistema stellare e dal quale fuoriescono segnali misteriosi.
        \item \emph{Taethi-9}: Un piccolo asteroide che ospita il monastero della Fede Ultima, un nuovo culto che sta raccogliendo moltissimi fedeli.
        \item \emph{Huroboros}: Una enorme creatura fuoriuscita dalla Singolarità, più grande di un pianeta.
            Erra tra i sistemi stellari in modo apparentemente innocuo.
\end{itemize}}
la tua intenzione è di giocare in tempi brevi in un'\glo{ambientazione} creata da zero, assicurati di definire l'idea che sta alla sua base come indicato a inizio di questo capitolo.
Se hai già un'idea di come procedere per dettagliare l'\glo{ambientazione} segui il tuo metodo, l'importante è che funzioni per te.
Se invece vuoi sperimentare una via veloce per creare il mondo di gioco, segui questo procedimento:
\begin{enumerate}
    \item \emph{Disegna una mappa.}
        È indifferente che si tratti della mappa di una città, di una regione, di una nazione, di un mondo o di una galassia.
        La scala della mappa dipende dal tono dell'\glo{ambientazione}.
        Non deve essere bella, sarà sufficiente che sia comprensibile a te che la stai disegnando.
        Un foglio di carta al centro del tavolo o una lavagna digitale andranno benissimo.
    \item \emph{Popola la mappa di luoghi importanti.}
        Generane non meno di tre e non più di dieci, usando dei segnalini o dei post-it.
    \item \emph{Definisci perché i luoghi sono importanti e chi li abita.}
        Cerca di dare la stessa attenzione a tutti i luoghi segnati sulla mappa, anche se hanno dimensioni e scale diverse.
        Non è un problema se un intero sistema solare è descritto dallo stesso numero di dettagli di un villaggio di pochi abitanti.
    \item \emph{Sviluppa una rete basilare di relazioni.}
        Collega gli individui che abitano i luoghi che hai descritto.
        Definisci per sommi capi chi sono e in che relazione sono gli uni con gli altri.
    \item \emph{Se ci sono equilibri precari in gioco, spezzali.}
        Crea dei conflitti in atto.
        Se ci sono legami stabili, chiediti cosa potrebbe farli vacillare.
        Crea delle fonti di tensione nell'\glo{ambientazione}.
        Scontri, pericoli, risorse connesse.
    \item \emph{Chiediti
        \marginpar{Si dice che l'annessione dei Korr sia una mossa dell'Unione per vantare diritti su Sigma-13.
            La Galassia è terrorizzata da Huroboros e il clima di fervore religioso aggrava la situazione.
            Gli \glo{eroi} vivono di missioni finanziate dal'Unione e si trovano in ricognizione nei pressi di Rea-Ko.}
        dove si trovano gli \glo{eroi} e perché sono lì.}
        Dopodiché prenditi il tempo che ti serve per far quadrare queste informazioni, che si tratti di minuti o di qualche giorno.
\end{enumerate}

Ovviamente puoi seguire questo procedimento anche assieme ai \glo{giocatori}, creando anche gli \glo{eroi} prima di completare l'ultimo punto.

\section{Crea degli esempi di \glo{tratti}}
Quando
\marginpar{Gianni e il suo gruppo di gioco sono soddisfatti dell'\glo{ambientazione} che hanno creato e decidono di metterla a disposizione a chiunque voglia giocarla.
    Stendono quindi dei \glo{tratti} di esempio, concentrandosi su quelli più caratteristici.

    Infatti, in ogni \glo{ambientazione} c'è qualcuno bravo a \emph{Convincere}, \emph{Interrogare} o \emph{Nascondersi}.
    Invece \emph{Addestramento a gravità zero} non è normalmente un \glo{tratto} che ogni persona può selezionare, ma potrebbe essere un'opzione plausibile in un'ambientazione fantascientifica, o addirittura comune se la \glo{storia} è ambientata per la maggior parte del tempo nello spazio.

    \begin{itemize}
        \item Esempi di archetipi: \emph{Prelato della Fede Ultima}, \emph{Politico dell'Unione}, \emph{Pirata dell'Orlo}, \emph{Frammento cosciente di Huroboros}, \emph{Separatista Korr}.
        \item Esempi di qualità: \emph{Ribelle}, \emph{Politicizzato}, \emph{Militante}, \emph{Fedele}, \emph{Impostore}, \emph{Visionario}, \emph{Forgiato}, \emph{Sintetico}
        \item Esempi di abilità: \emph{Pilotare}, \emph{Etichetta}, \emph{Contrabbando}, \emph{Estrazioni minerarie}, \emph{Fisica quantistica}, \emph{Combattimento a gravità zero}
        \item Esempi di \glo{tratti} alternativi: \emph{Toccato dalla Singolarità}, \emph{Difenderò il mio pianeta}, \emph{Fantasma dalla rete}, \emph{Specista}
\end{itemize}}
crei una nuova \glo{ambientazione} potrebbe essere utile elencare alcuni esempi di \glo{tratti}.
Questo non è un passaggio necessario: seguendo i punti precedenti, probabilmente hai già dato ai tuoi \glo{giocatori} informazioni sufficienti per avere un'idea solida del mondo che ospiterà i loro \glo{eroi}.

A seconda della familiarità che hanno con gli stereotipi dell'\glo{ambientazione}, potrebbero avere già in mente chi sarà il loro \glo{eroe}.
Inoltre questo passaggio è superfluo se la creazione degli \glo{eroi} viene fatta assieme.
Invece, se gli \glo{eroi} vengono creati singolarmente o se stai stendendo l'\glo{ambientazione} affinché venga utilizzata da altri gruppi di gioco, puoi dare loro qualche suggerimento.

La lista di \glo{tratti} che stilerai non sarà mai esaustiva: sicuramente alcuni \glo{giocatori} avranno idee per \glo{tratti} a cui non avevi pensato.
Questo è un bene!
Usa l'elenco che hai preparato come guida per consigliarti al meglio nella creazione di un \glo{eroe} ben calato nell'avventura.

Un elenco di \glo{tratti} ti aiuta a stabilire quali capacità ci si può aspettare dai personaggi.
Alcuni \glo{tratti} sono universali, altri non lo sono.
Capire quali \glo{tratti} sono comuni, rari o inesistenti ti aiuterà anche a inquadrare che genere di avversari e \glo{personaggi secondari} popoleranno l'\glo{ambientazione}.

\begin{itemize}
    \item Quali professioni e capacità sono comuni nel mondo di gioco?
    \item Quali professioni e capacità sono abbastanza rare da essere fonte di meraviglia?
    \item C'è qualche abilità che si è persa o che non è mai stata sviluppata?
    \item Quali sono i limiti invalicabili della tecnologia o della magia presente nell'\glo{ambientazione}?
    \item Quali elementi fantastici sono talmente comuni da essere parte della vita quotidiana?
    \item Ci sono elementi fantastici visti in modo particolare, sottoposti a leggi o restrizioni?
    \item Quali stereotipi della fiction sono più in linea con l'\glo{ambientazione}?
\end{itemize}

\section{Crea degli esempi di \glo{risorse}}
Un
\marginpar{Gianni si documenta rileggendo i suoi romanzi di fantascienza preferiti e si rifà a qualche serie televisiva.
    Aggiusta il tiro chiedendosi a quali situazioni in gioco dare spazio e stila questa lista:
    \begin{itemize}
        \item \glo{Risorse} comuni: trasporto planetario, cibo da coltura idroponica, chip di credito, med-bot di sostentamento vitale, comunicatore
        \item \glo{Risorse} rare: fucile soppressore a zero-g, med-bot di grado militare, astronave civile, cella di carburante per salto quantico, codici criptati dell'Unione
        \item \glo{Risorse} speciali: Astronave da guerra stealth non registrata, frammento di Huroboros, intelligenza artificiale risvegliata, reliquia originata dalla Singolarità
    \end{itemize}}
elenco di \glo{risorse} ti aiuterà nel corso della \glo{storia} a stabilire quanto è impegnativo ottenere qualcosa e quanto grave è perderlo.
Come per i \glo{tratti}, questa distinzione aiuta a visualizzare l'\glo{ambientazione} nel suo quotidiano e a renderla più solida.

\begin{itemize}
    \item \emph{Elenca le \glo{risorse} comuni:} quelle accessibili più o meno a tutti nell'\glo{ambientazione}.
        Costano poco, sono ampiamente reperibili con sforzo minimo.
    \item \emph{Elenca le \glo{risorse} rare:} il possesso di \glo{risorse} rare non è dato per scontato.
        Che sia per costo o difficoltà di reperibilità, il possesso di una \glo{risorsa} di questo tipo può essere motivo di invidia o un vantaggio significativo su chi non la possiede.
    \item \emph{Elenca le \glo{risorse} speciali:} la stragrande maggioranza della popolazione vivrà la sua vita senza mai neanche toccare una \glo{risorsa} speciale, figuriamoci possederla.
        Il possesso di una \glo{risorsa} speciale può alterare gli equilibri della \glo{storia}.
\end{itemize}

\section{Crea degli esempi di \glo{nomi}}
Per
\marginpar{Gianni stende l'elenco dei nomi delle principali specie annesse all'Unione Galattica e per ognuna di esse stilerà una lista di una decina di nomi tipici.

    A questa aggiunge alcuni nomi di luoghi comuni nel sistema in cui partirà la \glo{storia} e di una decina di corporazioni e gruppi.}
quanto possa sembrare banale, i nomi contribuiscono moltissimo a rendere viva un'\glo{ambientazione}.
I nomi sono potenti, sono uno dei riflessi di una cultura e del suo modo di vedere le cose.

Un elenco di nomi di soggetti e luoghi renderà più semplice al \glo{narratore} improvvisare situazioni credibili e in linea con il tono della narrazione; aiuterà inoltre i \glo{giocatori} a immergersi nel mondo che stanno esplorando.

Se l'\glo{ambientazione} comprende numerose specie, razze o culture e non hai tempo da dedicare a questi elenchi, concentrati su nomi dei soggetti e dei luoghi che gli \glo{eroi} avranno più probabilità di incontrare durante le prime \glo{sessioni}.

\section{Crea degli esempi di \glo{sfide}}
Il
\marginpar{Gianni vuole creare come \glo{sfida} di esempio una ciurma di pirati Korr.
    Nel rispondere alle domande ``Quali \glo{sventure} e \glo{complicazioni} è più probabile che causi?'' ``Quali sono le sue debolezze?'' e ``Come agisce?'' si rende conto di sapere davvero poco di pirateria, abbordaggi o di come funzioni e agisca una ciurma.

    Dopo qualche ricerca, al termine della stesura, Gianni si sente molto più preparato sull'argomento e più sicuro di poterlo narrare.}
\namecref{chap:vita-sfide} \nameref{chap:vita-sfide} nella \namecref{part:narrare-storie} \nameref{part:narrare-storie} elenca tutte le informazioni necessarie a creare \glo{sfide}.
Usa la creazione delle \glo{sfide} come un pretesto per documentarti sull'\glo{ambientazione} e imparare qualcosa di nuovo.

Creare una \glo{sfida} consiste nel rispondere a una lista di domande, ed è proprio la ricerca necessaria a dare risposte significative a costituire il vero valore di questo lavoro di stesura.

Ovviamente creare esempi di \glo{sfide} è anche un riferimento utile a chiunque voglia utilizzarle durante le \glo{sessioni}, o prenderne spunto per crearne di nuove mantenedo il tono dell'\glo{ambientazione}.

\section{Crea degli esempi di \glo{eroi}}
Questo
\marginpar{Nell'\glo{ambientazione} di Gianni piloti e agenti dell'Unione sono la scelta più ovvia, ma anche separatisti o membri della Fede Ultima potrebbero creare ottimi spunti per la \glo{storia}.}
passo è raccomandato unicamente se vuoi rendere fruibile l'\glo{ambientazione} a terze parti.
Non investire troppo tempo nel creare \glo{eroi} pregenerati: creare \glo{eroi} in \nte{} è veloce ed è parte del divertimento.
Spendi invece del tempo nel dare degli spunti per creare degli \glo{eroi} ben inseriti nel contesto della \glo{storia}.

\section{Sii sempre pronto a improvvisare}
Quando hai a che fare con materiale già pronto non cadere nel tranello di farti limitare da esso.
Avere materiale preparato non è un buon motivo per limitare i \glo{giocatori} ad agire unicamente entro i confini che ti offre.

In \nte{} improvvisare una \glo{sfida} o una \glo{prova} è semplice, basta abituarsi a farlo.
Tutto quello che ti serve è avere sott'occhio le scale di \glo{difficoltà} e \glo{pericolo}.
Descrivere una situazione sarà sufficiente a determinare gli impatti sulle regole.
Ascolta i \glo{giocatori} e le \glo{complicazioni} arriveranno da sole mano a mano che familiarizzi con l'\glo{ambientazione} e ti immergi nella \glo{storia}.
