% !TEX encoding = utf8
% !TEX program = pdflatex
% !TEX root = ../not-the-end
% !TEX spellcheck = it-IT

\chapter{Dare vita alle \glo{sfide}}
\label{chap:vita-sfide}
Durante il corso della \glo{storia}, il \glo{narratore} potrebbe decidere di arricchire di dettagli oggetti o situazioni con cui gli \glo{eroi} hanno a che fare, in modo da dare loro spessore e renderli più interessanti.

Persone, luoghi, oggetti o intere situazioni sono tutte considerate \glo{sfide} dal momento in cui gli \glo{eroi} dovranno effettuare \glo{prove} che li coinvolgono.

\nte{} utilizza il linguaggio naturale e universale per descrivere ogni tipo di \glo{sfida} in gioco.

Per dare vita a una sfida tieni presenti i seguenti suggerimenti.

\begin{enumerate}
    \item \emph{Descrivi in breve la sfida.}
        Dalle un nome breve e scrivi un breve testo introduttivo che descrive di cosa si tratta e quali sono le sue caratteristiche salienti.
    \item \emph{Indica quanto è pericolosa e difficile.}
        Dai una \glo{difficoltà} e un \glo{pericolo} alla \glo{sfida} utilizzando questa formula: <<Affrontare questa \glo{sfida} è \glo{difficile} \tokenfail{(1-6)}, \glo{esci di scena} se \glo{estrai} \tokenfail{(1-4)}>>.
    \item \emph{Elenca le eccezioni al punto precedente.}
        Elenca \glo{difficoltà} e \glo{pericolo} delle \glo{prova} che costituiscono le principali eccezioni al punto precedente, basandoti sulle caratteristiche uniche della \glo{sfida}.
    \item \emph{Elenca sventure e complicazioni più comuni.}
        Fai un breve elenco delle \glo{sventure} e delle \glo{complicazioni} più probabili o plausibili che potrebbero emergere nell'affrontare la \glo{sfida}.
    \item \emph{Approfondiscila a piacere.}
        Scegli quali delle domande di approfondimento che trovi nella prossima pagina sono più adeguate per dettagliare la sfida e segna le risposte.
\end{enumerate}

\section{Descrivere una \glo{sfida}}
Rispondi a una lista di domande, approfondendo a piacere le informazioni che ritieni adeguate a descrivere in modo efficace la \glo{sfida}.

\begin{enumerate}
    \item \emph{Qual
        \marginpar{Melchor è un adroide da combattimento.
            I suoi muscoli guizzano sotto la corazza subdermale e una katana monofilare brilla nella sua mano.}
        è il suo nome? Chi o cosa è? Che aspetto ha?}
        Queste domande servono a creare una descrizione sintetica della \glo{sfida}.
    \item \emph{In generale, quanto è \glo{difficile} e \glo{pericolosa} da affrontare?}
        Attribuisci
        \marginpar{Affrontare Melchor è \emph{difficile} (\tokenfail{4}) e \emph{molto pericloso} (\glo{esci di scena} se \glo{estrai} \tokenfail{2}).}
        una \glo{difficoltà} e un \glo{pericolo} di ordine generale alla \glo{sfida}, applicabili alla maggioranza delle \glo{prove}.
    \item \emph{Quali
        \marginpar{Corrompere Melchor è \emph{impossibile}, persuaderlo utilizzando la logica è \glo{facile} (\tokenfail{2}) e \emph{per niente pericoloso}.}
        sono le eccezioni principali alla domanda precedente?}
        Elenca le principali \glo{prove} che caratterizzano i punti di forza e di debolezza della \glo{sfida}.
    \item \emph{Quali
        \marginpar{Melchor è letale in corpo a corpo, inoltre riesce a controllare i sistemi di sicurezza e supporto vitale degli edifici (porte, ventilazione, \textellipsis).}
        \glo{sventure} e \glo{complicaziono} è più probabile che causi?}
        Elenca brevemente le principali \glo{sventure} e \glo{complicazioni} correlate alla \glo{sfida}, in modo da averle velocemente come riferimento durante la narrazione.
    \item Scegli
        \marginpar{La coscienza di Melchor è la digitalizzazione dei ricordi del padre di Markus Finnigan, l'ingegnere responsabile del progetto che lo ha creato.

        La direttiva principale di Melchor è quella di proteggere il \foreignlanguage{english}{mainframe} della Cronotech, ma risvegliare i ricordi del padre di Markus potrebbe attivare la sua coscienza latente.

        Voltaggi elevati rallentano i movimenti di Melchor, sovraccaricando i suoi sistemi, ma rendendolo anche pazzo di rabbia.}
        tra le seguenti domande di approfondimento.
        \begin{itemize}
            \item Quali sono i suoi segreti?
            \item Quali sono le sue debolezze?
            \item Qual è la sua ragion d'essere?
            \item Chi sa tutto sul suo conto?
            \item Dove si trova?
            \item Come agisce?
            \item Quali sono le sue origini?
            \item Quali momenti hanno definito ciò che è?
            \item Cosa custodisce di prezioso?
            \item Chi farebbe di tutto per il suo bene o per la sua rovina?
        \end{itemize}
\end{enumerate}

\subsection{Quando e quanto è giusto dettagliare una \glo{sfida}}
\marginpar{Claudio ha deciso di dettagliare le guardie del Barone con un'unica descrizione, ma durante la narrazione una di esse è diventata rilevante per la \glo{storia} e Claudio decide di approfondirla con una descrizione dedicata.}
Dettaglia le \glo{sfide} nella misura in cui tu e i \glo{giocatori} le considerate interessanti o importanti per la \glo{storia}.
Puoi dettagliare le \glo{sfide} in modo parziale, rispondendo solo ad alcune domande e tralasciando le successive.
Le domande sono elencate in ordine di importanza, quindi se vuoi rispondere solo ad alcune prediligi le prime.

\subsection{Che tipo di \glo{sfide} posso descrivere?}
\marginpar{In termini di gioco, descrivere uno spadaccino, delle segrete o un veicolo militare non cambia.
    Potrai utilizzare le stesse regole di creazione per descrivere qualunque soggetto con cui i \glo{giocatori} si relazioneranno.}
Puoi descrivere \glo{sfide} di ogni genere!
Le domande di supporto sono formulate in modo da funzionare da spunto per qualsiasi tipo di soggetto, sia esso un individuo, un gruppo, una situazione, una fazione o un oggetto.

\subsection{Come utilizzo la descrizione della \glo{sfida}?}
Fai in modo che ciò che scrivi quando dettagli una \glo{sfida} ti sia di supporto senza vincolarti eccessivamente.
L'idea generale che hai della \glo{sfida} è la cosa più importante, perché ti dà un'idea istintiva di come improvvisare \glo{difficoltà} e \glo{pericolo} di qualsiasi prova che la riguardi.

Tutte le domande successive sono spunti, ma non devono mai frenare la narrazione.
Le \glo{prove}, come le \glo{sfide} e \glo{complicazioni} che ne derivano, dipendono tanto dalla \glo{sfida} tanto dal contesto in cui si svolgono e dalla \glo{scena}.

\subsection{Come determino \glo{difficoltà} e \glo{pericolo}?}
Pensa
\marginpar{Un branco di lupi sono una sfida ardua per dei contadini del basso medioevo, mentre sono solo una seccatura per dei semidei dell'antica Grecia.}
al contesto narrativo in cui la \glo{sfida} si inserisce.
Lo stesso soggetto in filoni narrativi diversi potrebbe essere più o meno \glo{difficile} e \glo{pericoloso} da affrontare.
Quando approfondisci una \glo{sfida} considera il tipo di \glo{storia} che state narrando.

\section{Alcuni esempi di \glo{sfide}}
Nelle prossime pagine troverai alcuni esempi di \glo{sfide}.
Nello specifico una persona, un luogo e un oggetto.
La \namecref{part:ambientazioni} \nameref{part:ambientazioni} di esempio contiene molti altri esempi che potrai esplorare.

Ogni \glo{sfida} è dettagliata da:

\begin{itemize}
    \item breve descrizione generale;
    \item \glo{difficoltà} e \glo{pericolo} delle \glo{prove};
    \item suggerimenti di \glo{sventure};
    \item suggerimenti di \glo{complicazioni};
    \item approfondimenti.
\end{itemize}

Puoi sfruttare questa struttura per presentare in modo sintetico le risposte alle cinque domande utili a descrivere le \glo{sfide}.

\subsection{Il Barone Cornelius}
Il Barone Cornelius è un uomo sulla cinquantina.
Ha occhi penetranti e un volto severo che si scioglie solo quando indulge nel bere.
È evidentemente un militare di carriera.

\subsubsection{\glo{Difficoltà} e \glo{pericolo} delle \glo{prove}}
\begin{itemize}
    \item Affrontarlo è \emph{difficile} (\tokenfail{4}) e \emph{abbastanza pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{3})
    \item Affrontarlo in duello è \emph{difficilissimo} (\tokenfail{5}) e \emph{molto pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{2})
    \item Il Barone è facile all'ira, fargli perdere le staffe è \emph{molto facile} (\tokenfail{1})
\end{itemize}

\subsubsection{Possibili \glo{sventure}}
\begin{itemize}
    \item \emph{Disarmato}, o qualsiasi tipo di \emph{Ferita} fisica se affrontato in combattimento.
    \item \emph{Condannato} o \emph{Ricercato}, se lo si obbliga a far valere la sua autorità.
    \item \emph{Malvisto a corte}, se lo si prende di petto in situazioni sociali.
\end{itemize}

\subsubsection{Possibili \glo{complicazioni}}
\begin{itemize}
    \item Degli armigeri arrivano a dare manforte.
    \item Il Barone si mette nella guardia del Falcone, in cui è famigerato per la sua letalità.
    \item Il Barone sfogherà la sua rabbia sulla gente comune o su qualcuno di indifeso.
    \item Gli \glo{eroi} si fanno dei nemici a corte.
\end{itemize}

\subsubsection{Approfondimenti}
Il Barone Cornelius disprezza l'indole sensibile di suo figlio, su cui riversa la rabbia che prova per la scomparsa di sua moglie, morta dandolo alla luce.
Si è conquistato il titolo nobiliare combattendo sul campo.
Desidera gloria per la famiglia Cornelius.
Vuole un altro figlio maschio, ma teme la prospettiva di innamorarsi ancora.
Ama dare banchetti nei quali tende a bere troppo, rievocare aneddoti del suo passato e parlare a sproposito.
Suo figlio è terrorizzato da lui, ma farebbe di tutto per la sua approvazione.

\subsection{Il Dedalo del gelido tormento}
Un labirinto di pietra, ghiaccio e metallo arrugginito in cui la luce del sole non arriva mai.
Il Dedalo è un insieme di celle e tunnel tentacolari che fungono da colonia prigione sulla superficie di Ganimede.

\subsubsection{\glo{Difficoltà} e \glo{pericolo} delle \glo{prove}}
\begin{itemize}
    \item Affrontare \glo{prove} all'interno del Dedalo è \emph{difficile} (\tokenfail{4}) e \emph{molto pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{2}).
    \item Intimidire le guardie cibernetiche del Dedalo è \emph{impossibile}.
    \item Le serrature delle celle sono vecchie e usurate dal freddo, forzarle è \emph{facile} (\tokenfail{2}) e \emph{per niente pericoloso}.
    \item Trovare l'uscita dal Dedalo è \emph{quasi impossibile} (\tokenfail{6}).
\end{itemize}

\subsubsection{Possibili \glo{sventure}}
\begin{itemize}
    \item \emph{Disorientato}
    \item \emph{Assiderato}
    \item \emph{Braccato}
\end{itemize}

\subsubsection{Possibili \glo{complicazioni}}
\begin{itemize}
    \item Scatta un allarme.
    \item Gli \glo{eroi} vengono scoperti da due guardie cibernetiche.
    \item Una paratia di sicurezza scatta all'improvviso e separa il gruppo.
    \item Gli \glo{eroi} scoprono di avere dei nemici tra i detenuti del Dedalo.
\end{itemize}

\subsubsection{Approfondimenti}
In origine, il Dedalo nasce come una colonia di estrazione e raffinamento del ghiaccio.
La struttura è stata in seguito rilevata dalla SecurCorp, che l'ha riqualificata come centro di detenzione.
Lontano dallo sguardo dei pianeti centrali, le politiche del Dedalo si sono fatte sempre più spietate.
Karl Hansen fu il capo ingegnere dei progetti originali del Dedalo.

\subsection{L'Anello di Samarcanda}
Una semplice fascia di argento con una incisione in uno strano alfabeto all'interno.
Ogni volta che si indossa l'anello si avvertono strane sensazioni: pelle d'oca, sussurri e la strana voglia di non smettere di indossarlo.

\subsubsection{\glo{Difficoltà} e \glo{pericolo} delle prove}
\begin{itemize}
    \item Le \glo{prove} che riguardano l'Anello sono \emph{medie} (\tokenfail{3}) e \emph{poco pericolose} (\glo{esci di scena} \glo{estraendo} \tokenfail{4}).
    \item Decifrare le scritte all'interno dell'anello è \emph{difficilissimo} (\tokenfail{5}).
    \item Pronunciare la formula una volta decifrate le scritte è \emph{facilissimo} (\tokenfail{1}) e \emph{estremamente pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{1}).
\end{itemize}

\subsubsection{Possibili \glo{sventure}}
\begin{itemize}
    \item Assuefatto all'Anello
    \item Soggiogato dalle anime dell'Anello
\end{itemize}

\subsubsection{Possibili \glo{complicazioni}}
\begin{itemize}
    \item Qualcuno in cerca dell'Anello è sulle tracce degli \glo{eroi}, ed è disposto a tutto pur di recuperarlo.
    \item Uno degli spiriti dell'Anello si libera e comincia a tormentare il gruppo.
    \item Le informazioni per decifrare le iscrizioni sono custodite in territorio nemico.
\end{itemize}

\subsubsection{Approfondimenti}
L'Anello di Samarcanda è un oggetto leggendario opera di un mago che ha sacrificato molte vite per conferirgli potere.
Le anime delle vittime sacrificate risiedono ora nell'Anello, e cercheranno la compagnia di chi lo indossa.
Una volta pronunciate le parole magiche all'interno dell'Anello, le anime cercheranno di soggiogare chi lo indossa, ma se non ci riusciranno saranno obbligate a servirlo proteggendolo da attacchi mentali e da ogni forma di magia.

\section{\glo{Sfide} avanzate}
A volte alcune \glo{sfide} sono troppo importanti o particolari per gestirle usando unicamente \glo{sventure} e \glo{complicazioni} e potresti voler aggiungere delle regole speciali per rendere chiaro ai \glo{giocatori} quanto importanti siano.

In questo caso puoi pensare a uno o due modi particolari in cui i \glo{giocatori} potranno o dovranno spendere \tokenfail{}.

\subsection{Quando usare una \glo{sfida} avanzata}
La
\marginpar{Etienne ha inseguito l'assassino di sua moglie per mesi, incrociando la sua strada diverse volte, ma lui è riuscito ogni volta a fuggire.}
nemesi di uno degli \glo{eroi}, un \glo{personaggio secondario} molto approfondito e incontrato più volte o un avversario leggendario sono tutte \glo{sfide} che potrebbero diventare \glo{sfide avanzate}.
Una
\marginpar{Lothar ha sentito voci su Kathrax, uno stregone delle terre del nord che sembra poter plasmare le fiamme con la propria volontà.}
linea guida per decidere se una \glo{sfida} può essere \glo{avanzata} è chiedersi: quale aspetto di questa \glo{sfida} può essere particolarmente impegnativo da superare e quali condizioni particolari scateneranno delle consequenze inaspettate?

Non cercare di forzare la mano quando ti fai questa domanda, se la risposta non ti è chiara potrebbe semplicemente voler dire che per quanto \glo{difficile} o \glo{pericolosa} sia la situazione, \glo{sventure} e \glo{complicazioni} sono più che sufficienti a portare avanti la \glo{storia}.

\subsection{Come bilancio una \glo{sfida} avanzata?}
Le
\marginpar{Quando un \glo{eroe} viene aiutato mentre combatte le fiamme di Kathrax, se l'\glo{eroe} \glo{estrae} più di \tokenfail{1}, gli \glo{eroi} che l'hanno aiutato ricevono \tokenfail{2} invece che \tokenfail{1}.}
variazioni che applichi alle regole dovrebbero essere in linea con le \glo{lezioni} che gli \glo{eroi} possono imparare: spendere in maniera unica i \tokenfail{} \glo{estratti} o \glo{estrarre} \tokenrand{} in maniera diversa, senza togliere al \glo{giocatore} la libertà di spendere i \tokensucc{} e senza richiedere più di un \tokensucc{} per riuscire in un'azione.
Sono eccezioni, e come tali dovresti usarle solo quando fanno la differenza: abusandone rallenterai il gioco e renderai le \glo{sfide} molto meno interessanti per i \glo{giocatori}.

\subsubsection{Il Barone Cornelius}
Quando il Barone si trova nella guardia del Falcone, un \glo{eroe} deve \glo{estrarre} esattamente un numero di \tokenrand{} pari al numero di \glo{eroi} presenti in \glo{scena}, compreso sé stesso.
Se ci sono più \glo{eroi} in \glo{Scena} che \tokenrand{} nel \glo{sacchetto}, l'\glo{eroe} deve \glo{estrarre} il massimo numero di \tokenrand{} disponibile.
Un \glo{eroe} può \glo{rischiare} dopo aver \glo{estratto}, se rimangono abbastanza \tokenrand{}.

\subsubsection{Il Dedalo del gelido tormento}
Quando il \glo{narratore} riceve un \tokenfail{} per una \glo{complicazione} da un \glo{eroe}, può attivare il sistema di controllo remoto dei prigionieri, attivando il loro chip di soppressione.
Fino a quando non viene disattivato, gli \glo{eroi} all'interno del Dedalo devono scegliere un loro \glo{tratto} che non può essere usato a causa delle interferenze del chip con le loro memorie.

\subsubsection{L'Anello di Samarcanda}
Quando un \glo{eroe} assuefatto all'Anello \glo{estrae} almeno \tokenfail{3}, l'\glo{eroe} elimina la \glo{sventura} \emph{Assuefatto all'anello} e la segna come \glo{cicatrice} temporanea, che rimane fino a quando qualcun altro indossa l'Anello.
Non basta perdere l'Anello per rimuovere la \glo{cicatrice}, è necessario che l'Anello venga indossato da qualcun altro.
