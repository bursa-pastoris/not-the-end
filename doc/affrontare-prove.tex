% !TEX encoding = utf8
% !TEX program = pdflatex
% !TEX root = ../not-the-end
% !TEX spellcheck = it-IT

\chapter{Affrontare le \glo{prove}}
\label{chap:affrontare-prove}
Per giocare a \nte{} devi comprendere lo spirito con cui vanno affrontate le \glo{prove}.

Ogni \glo{prova} corrisponde a un'opportunità di esplorare i moventi del tuo \glo{eroe}, narrarne le imprese e regalare al \glo{narratore} spunti per movimentare la \glo{storia}.

Approfondirai tutti i passi necessari ad affrontare le \glo{prove} di cui abbiamo già parlato nella \namecref{part:introduzione} \nameref{part:introduzione}, e a sfruttarle come dei momenti chiave per arricchire la narrazione.

Per affrontare una prova devi seguire questi passaggi.

\begin{enumerate}
    \item \emph{Descrivi l'obiettivo.}
        Descrivi l'obiettivo dell'\glo{eroe} e le azioni con cui cerca di ottenerlo, in modo da rendere chiaro quali \glo{tratti} metterai in gioco.
    \item \emph{Componi il sacchetto.}
        Dovrai inserire tanti \tokensucc{} quanti sono i \glo{tratti} che metti in gioco e tanti \tokenfail{} quant'è la \glo{difficoltà} indicata dal \glo{narratore}.
    \item \emph{Estrai da 1 a 4 \tokenrand{} a tua scelta.}
        I \tokenrand{} vanno \glo{estratti} alla cieca e tutti in una volta.
        Più \tokenrand{} \glo{estrarrai}, più sarà probabile ottenere \glo{successi} e \glo{complicazioni}.
    \item \emph{Decidi se \emph{rischiare}.}
        Se non sei soddisfatto dei \tokenrand{} che hai \glo{estratto}, puoi \glo{rischiare} ed \glo{estrarne} altri dal \glo{sacchetto}.
        Quando \glo{rischi} devi sempre arrivare ad avere un totale di \tokenrand{5} \glo{estratti}.
     \item \emph{Spendi \tokenrand{} e racconta l'esito.}
        Spendi \tokensucc{} per superare la \glo{prova} o migliorare i \glo{tratti} e \tokenfail{} per importi \glo{adrenalina} o \glo{confusione}, o chiedere al \glo{narratore} di importi \glo{sventure} o \glo{complicare} la \glo{scena}.
        Se hai deciso di \glo{rischiare}, il \glo{narratore} spende i \tokenfail{} al posto tuo.
\end{enumerate}

\section{Cos'è una \glo{prova}}
Quando il \glo{giocatore} ha un obiettivo il cui esito è incerto o importante per la \glo{storia}, il \glo{narratore} gli chiede di affrontare una \glo{prova}.
Quando richiede una \glo{prova}, il \glo{narratore} comunica al \glo{giocatore} quanto è \glo{difficile} e quanto è \glo{pericolosa}.

\subsection{Quando è necessario affrontare una prova}
Ogni \glo{prova} è un momento di svolta per la \glo{storia} che potrebbe risultare in \glo{successi} e \glo{complicazioni}.
Ogni \glo{prova} corrisponde a un'opportunità di esplorare i moventi degli \glo{eroi}, narrarne le imprese e regalare al \glo{narratore} spunti per movimentare la \glo{storia}.
Di norma affronti una \glo{prova} quando:
\begin{itemize}
    \item non
        \marginpar{Sgattaiolare non visto tra le guardie di palazzo, hackerare un sistema di sicurezza, convincere uno sconosciuto ad aiutarti, duellare con un avversario, disinnescare una bomba, sono tutti casi che richiedono una \glo{prova}.}
        c'è la certezza del fatto che l'\glo{eroe} avrà successo in ciò che sta tentando di fare;
    \item esiste una possibilità concreta che si creino \glo{complicazioni} interessanti o significative per lo sviluppo della \glo{storia};
    \item c'è una posta in gioco, o un conflitto sociale è in atto.
\end{itemize}

È meglio evitare di affrontare una \glo{prova} se:
\begin{itemize}
    \item l'azione
        \marginpar{
            Prendere un oggetto incustodito o conoscere gli usi o costumi di un luogo, salve rare eccezioni, non sono casi che richiedano una \glo{prova}.

            Ad esempio Lilian, essendo \emph{Istruita} e abile a \emph{Guarire}, non avrà bisogno di affrontare \glo{prove} per diagnosticare una malattia.
            Il \glo{narratore}, dopo aver verificato i suoi \glo{tratti}, racconterà ciò che Lilian sa del male che affligge il suo paziente.
        }
        è talmente scontata da non avere possibilità di fallimento, o totalmente irrealizzabile da non avere possibilità di successo;
    \item non c'è alcuna possibilità che si creino \glo{complicazioni} interessanti per lo sviluppo della \glo{storia};
    \item il \glo{giocatore} vuole verificare se il suo \glo{eroe} conosce qualcosa, qualcuno o se ha notato qualcosa.
        In questo caso è meglio che il \glo{narratore} rispodna al \glo{giocatore} facendo riferimenti ai \glo{tratti} dell'\glo{eroe}.
\end{itemize}

\section{\glo{Difficoltà} e \glo{pericolo} delle \glo{prove}}
Ogni
\marginpar{\glo{Difficoltà} e \glo{pericolo} sono valori distinti.
    Decodificare un messaggio cifrato è \emph{difficile} ma \emph{per niente pericoloso}, disinnescare un esplosivo rudimentale è \emph{facile} ma \emph{estremamente pericoloso}.}
\glo{prova} ha un valore di \glo{difficoltà}.
Inoltre alcune \glo{prove} hanno un \glo{pericolo}.
Se sei il \glo{narratore}, consulta la \namecref{part:narrare-storie} \nameref{part:narrare-storie} per scoprire in dettaglio come gestire questi due valori.

\subsection{\glo{Difficoltà}}
La
\marginpar{Lothar cerca di convincere alcune guardie a dargli delle informazioni.
    Il \glo{narratore} ritiene la \glo{prova} \emph{difficile} e comunica che la \glo{difficoltà} della \glo{prova} è +4.
    Lothar aggiunge \tokenfail{4} al \glo{sacchetto}.}
\glo{difficoltà} è un valore che descrive quanto è probabile che la \glo{prova} generi delle \glo{complicazioni}.
Quando il \glo{giocatore} affronta una \glo{prova} deve inserire un numero di \tokenfail{} pari alla \glo{difficoltà} dichiarata dal \glo{narratore}.

\begin{itemize}
    \item Facilissima: \tokenfail{1}
    \item Facile: \tokenfail{2}
    \item Normale: \tokenfail{3}
    \item Difficile: \tokenfail{4}
    \item Difficilissima: \tokenfail{5}
    \item Quasi Impossibile: \tokenfail{6}
\end{itemize}

\subsection{\glo{Pericolo}}
Il
\marginpar{Lothar desidera disinnescare una trappola a pressione su cui ha appena posato un piede per errore.
    Il \glo{narratore} comunica che la \glo{prova} è \emph{difficile} (\tokenfail{4}) e \emph{molto pericolosa} (\tokenfail{2}).
    Lothar aggiunge \tokenfail{4} al \glo{sacchetto} e sa che se \glo{estrarrà} 2 o più \tokenfail{} \glo{uscirà di scena}.}
\glo{pericolo} è un valore che descrive la probabilità che l'\glo{eroe} \glo{esca di scena} affrontando una \glo{prova}.
L'\glo{eroe} \glo{esce di scena} se il \glo{giocatore} \glo{estrae} un numero di \tokenfail{} pari o superiore al \glo{pericolo} della \glo{prova} dichiarato dal \glo{narratore}.

\begin{itemize}
    \item Per niente pericolosa: non è possibile \glo{uscire di scena}
    \item Poco pericolosa: \glo{esci di scena} \glo{estraendo} \tokenfail{4}
    \item Abbastanza pericolosa: \glo{esci di scena} \glo{estraendo} \tokenfail{3}
    \item Molto pericolosa: \glo{esci di scena} \glo{estraendo} \tokenfail{2}
    \item Estremamente pericolosa: \glo{esci di scena} \glo{estraendo} \tokenfail{1}
\end{itemize}

Trovi tutti i dettagli sull'\glo{uscire di scena} più avanti nel \namecref{chap:affrontare-prove}, per ora è sufficiente sapere che \glo{uscire di scena} equivale a non essere più in grado di agire al suo interno.

\section{Descrivere l'\glo{obiettivo}}
Quando affronti una \glo{prova}, descrivi l'obiettivo dell'\glo{eroe} e le azioni con cui cerca di ottenerlo, in modo da rendere chiaro quali \glo{tratti} metterà in gioco.

Sebbene
\marginpar{\emph{``Voglio intrufolarmi di soppiatto nel castello.
    Sgattaiolo tra le fila nemiche, guardandomi attorno per tenere d'occhio la posizione delle guardie di ronda e saltando di tetto in tetto quando non mi è possibile passare in sicurezza tra i vicoli''} è un'ottima descrizione per portare in gioco \glo{tratti} come \emph{Furtivo}, \emph{Circospetto}, \emph{Agile} e \emph{Infiltratore}.}
la cosa migliore sia quella di descrivere fin dall'inizio l'azione in modo che coinvolga i \glo{tratti} che desideri utilizzare, giustificare a posteriori l'utilizzo di un \glo{tratto} è comunque permesso.

Cercare di mettere in gioco più \glo{tratti} possibile è un esercizio lecito e divertente, ma è meglio evitare descrizioni tirate per i capelli pur di avere un \tokensucc{} in più nel \glo{sacchetto}.
È molto meglio immaginare cosa sta facendo il tuo \glo{eroe} e narrarlo, in modo che i \glo{tratti} messi in gioco emergano in modo naturale.

\subsection{Cosa faccio se fatico a raccontare le \glo{prove} che devo affrontare?}
Sebbene
\marginpar{Un modo meno scorrevole, ma altrettanto funzionale, di giocare consiste nel dire \emph{``Supero le guardie''}, il \glo{narratore} ti chiederà come lo fai e a quel punto racconterai come metti in gioco i tuoi \glo{tratti}.}
elencare i \glo{tratti} che utilizzi durante la \glo{prova} senza alcuna descrizione sia meno coinvolgente, è inutile sforzarsi di fare descrizioni auliche o adeguarsi allo stile narrativo dei giocatori più loquaci.

Lascia da parte ogni ansia da prestazione e descrivi come ti viene più naturale, non hai nessun motivo per sentirti vincolato a una forma precisa o uno stile a cui conformarti.

Eventualmente, usa i \glo{tratti} del tuo \glo{eroe} come ispirazione per descrivere ciò che stai facendo.
Ricordati inoltre di non forzare gli altri \glo{giocatori} a uniformarsi al tuo stile.

Sii di supporto a chi gioca con te facendo domande e interessandoti a ciò che racconta.
Non è necessario essere narratori professionisti per divertirsi assieme, le cose importanti sono il rispetto e l'ascolto reciproco.

\section{Comporre il \glo{sacchetto}}
Il \glo{giocatore} inserisce nel \glo{sacchetto} \tokensucc{1} per ogni \glo{tratto} messo in gioco, e un numero di \tokenfail{} pari alla \glo{difficoltà} scelta dal \glo{narratore}

Prendi dalla \glo{riserva} tanti \tokenfail{} quanto sono quelli che ti segnala il \glo{narratore} e mettili nel \glo{sacchetto}.
Dopodiché elenca i \glo{tratti} che metti in gioco e per ognuno di essi aggiungi \tokensucc{1} al \glo{sacchetto}, in modo che il gruppo di gioco sappia quali sono e tu possa verificare di non averne dimenticato nessuno.
Ricorda di svuotare il \glo{sacchetto} da eventuali \tokenrand{} rimasti durante \glo{prove} affrontate in precedenza prima di riempirlo.

\subsection{Se non ho \glo{tratti} da mettere in gioco come affronto la \glo{prova}?}
Se
\marginpar{È difficile che un \emph{Sacerdote}, \emph{Caritatevole}, \emph{Gentile} e \emph{Pacifista} si trovi nella condizione di voler far del male a qualcuno.
    Se capitasse, si troverebbe impreparato su come fare.
    Starà a lui interrogarsi sulla sua incompetenza e decidere se imparare a combattere o meno.

    Non avere \emph{Atletico} tra i \glo{tratti} non implica che tu non sia in grado di correre 200 metri.
    Solo le \glo{prove} complesse o in cui veramente si rischia qualcosa saranno impossibili.}
affronti una \glo{prova} e non hai nessun \glo{tratto} da portare in gioco, non hai possibilità di riuscire.
I \glo{tratti} descrivono non solo chi è il tuo \glo{eroe}, ma anche come tende ad agire.

Se ti rendi conto che al tuo \glo{eroe} mancano dei \glo{tratti} che a tuo avviso dovrebbe avere, potrai tentare di farlo evolvere in quella direzione.
La \namecref{part:evolvere-eroe} \nameref{part:evolvere-eroe} ti illustra come fare.

Accorgerti di una carenza del tuo \glo{eroe} è un ottimo stimolo per chiederti se l'\glo{eroe} non desideri diventare qualcosa di differente da quello che è al momento oppure se preferisce rimanere com'è, concentrandosi sui suoi punti forti.

Ovviamente questo ragionamento vale per \glo{prove} vere e proprie, che quindi hanno una \glo{difficoltà} e sono azioni non accessibili a chiunque.
La mancanza di un \glo{tratto} nell'alveare non ti rende del tutto incapace di affrontare anche i compiti più semplici in quell'ambito.

\section{Decidere quanto \glo{estrarre}}
Decidi quanti \tokenrand{} \glo{estrarre} tra 1 e 4.
Più \tokenrand{} \glo{estrarrai} più cose accadranno.
Quanto è probabile che siano positive o negative dipende dai \tokenrand{} che compongono il \glo{sacchetto}.

La
\marginpar{Poni il caso limite in cui ti abbia \tokensucc{1} e \tokenfail{3} nel \glo{sacchetto}.
    \glo{Estrarre} \tokenrand{4} ti darà la sicurezza di avere \glo{successo} nella \glo{prova}, al costo di far fronte a molte \glo{complicazioni}.
    Se la \glo{prova} che stai affrontando è molto importante per te, potresti essere disposto a pagare il prezzo pur di riuscire nel tuo intento.
    Subire una ferità che lascerà un segno per distruggere un malvagio artefatto renderà l'impresa del tuo \glo{eroe} più interessante e ti offrirà spunti di interpretazione nel vederlo evolvere e cambiare nel tempo.}
maggioranza dei \glo{sacchetti} avrà un buon equilibrio di \tokensucc{} e \tokenfail{}.
Spesso la voglia di riuscire ti porterà a decidere di subire \glo{complicazioni} per assicurarti di avere \glo{successo} nella \glo{prova} in cui ti stai cimentando.

Da un lato ti accorgerai che non è semplice avere \glo{successi} senza \glo{complicazioni}, ma scoprirai anche di essere spesso nella condizione di poter \glo{estrarre} abbastanza da assicurarti di avere almeno \tokensucc{1} nella tua mano.

Scoprirai di avere un grande livello di controllo nel gestire le \glo{complicazioni}, al punto tale che fare uso dei \tokenfail{} che \glo{estrarrai} ti risulterà divertente quanto spendere i \tokensucc{}.

In \nte{} le tue probabilità di successo sono indissolubilmente legate a quanto sei disposto a rischiare pur di ottenere ciò che desideri.

\subsection{Come scegliere quanti \tokenrand{} \glo{estrarre}?}
Dovresti
\marginpar{In un duello all'ultimo sangue, con un \glo{sacchetto} composto da \tokensucc{3} e \tokenfail{3}, \glo{estrarre} \tokenrand{4} equivale a cercarsi dei guai per avere la sicurezza di colpire l'avversario.
    Potresti addirittura avere dei motivi per desiderare che il tuo \glo{eroe} rimanga segnato.
    Poniamo il caso che l'\glo{eroe} stia cercado di disinnescare una bomba e il \glo{sacchetto} sia composto da \tokensucc{2} e \tokenfail{2}.
    \glo{Estrarre} \tokenrand{1} potrebbe essere un \glo{successo} o \glo{complicare} la \glo{scena}, ma non causerà danni eccessivi.}
\glo{estrarre} molti \tokenrand{} se pensi che il tuo \glo{eroe} sia disposto a far fronte a numerose \glo{complicazioni}, pur di ottenere il \glo{successo} che desidera, oppure se vuoi far accadere tante cose e movimentare la \glo{scena}.

Al contrario, dovresti \glo{estrarre} pochi \tokenrand{} quando il tuo \glo{eroe} è disposto a fallire pur di evitare rischi per se stesso o nella \glo{scena}.
Meno \tokenrand{} \glo{estrarrai}, meno conseguenze negative ci saranno nella \glo{scena}.

\glo{Estrarre} solo \tokenrand{1} significa che la \glo{prova} si risolverà unicamente bene o male, senza eccessive opportunità o complicazioni.
In ultimo, in una minoranza di casi, \glo{estrarre} \tokenrand{1} è un modo per dire \emph{``o la va o la spacca''} e ogni tanto è proprio la scelta giusta da fare.

\section{Decidere se \glo{rischiare}}
\label{sec:decidere-rischiare}
Puoi \glo{rischiare} per \glo{estrarre} altri \tokenrand{} oltre a quelli che hai già \glo{estratto} durante una \glo{prova}.

\subsection{Quando posso \glo{rischiare}?}
Il
\marginpar{Lothar vuole catturare un criminale che potrebbe testimoniare contro il figlio del Barone.
    Il fuggitivo cerca di distanziarlo correndo a perdifiato nella folla.
    Nel \glo{sacchetto} di Lothar ci sono \tokensucc{4} e \tokenfail{4}.

    Roberto decide di \glo{estrarre} \tokenrand{3}, e scopre con disappunto che si tratta di \tokenfail{3}.}
\glo{giocatore} può decidere di \glo{rischiare} durante qualsiasi \glo{prova}, subito dopo aver \glo{estratto} e prima di aver speso i \tokenrand{} per raccontare l'esito della \glo{prova}.

\subsection{Quale prezzo si paga per \glo{rischiare}?}
Quando
\marginpar{Lothar non può accettare di lasciarsi scappare il fuggitivo e decide quindi di \glo{rischiare}.
    Roberto \glo{estrae} \tokensucc{1} e \tokenfail{1}, nella sua mano quindi ha un totale di \tokensucc{1} e \tokenfail{4}.
    Con il \tokensucc{} si è assicurato il \glo{successo} nella \glo{prova}, ma il \glo{narratore} ha ora \tokenfail{4} da spendere come preferisce e decide di farlo imponendo a Lothar 1 \glo{sventura}, \glo{confusione} e \glo{adrenalina}, per poi \glo{complicare} la \glo{scena}.
    Il \glo{narratore} racconta come Lothar, per riuscire ad acciuffare il fuggitivo, butta a terra un passante che si rivela essere il capo delle guardie.
    Lothar subisce la \glo{sventura} \emph{Odiato dalle autorità}, e ora le guardie hanno messo il naso nella faccenda.
    Il battito cardiaco di Lothar aumenta all'impazzata, uscire da questa situazione sarà tutt'altro che facile.}
il \glo{giocatore} \glo{rischia} deve rispettare questi due vincoli:
\begin{itemize}
    \item con la seconda \glo{estrazione} si deve sempre arrivare ad avere un totale di \tokenrand{5} \glo{estratti};
    \item tutti i \tokenfail{} \glo{estratti} vanno consegnato al \glo{narratore}, che li spenderà al posto del \glo{giocatore} a suo piacimento.
\end{itemize}

Da un lato \glo{rischiare} di dà la possibilità di ribaltare le sorti di una \glo{prova} che non è andata come volevi, dall'altro dovrai tentare il tutto per tutto e pagherai il prezzo di non poter controllare gli esiti negativi che ne derivano.
\glo{Rischia} solo per ciò che il tuo \glo{eroe} considera davvero importante!

\section{Utilizzare i \glo{successi}}
Devi fare uso di tutti i \tokensucc{} che \glo{estrai}.
Puoi spenderli per avere \glo{successo} nella \glo{prova} o rafforzare un \glo{tratto}.
Ogni \tokensucc{} può essere speso separatamente.

\subsection{Avere \glo{successo} nella \glo{prova}}
Devi
\marginpar{Roberto vuole recidere con una freccia la corda legata al collo di un malcapitato.
    \glo{Estrae} \tokensucc{3}.
    Spende il primo, che gli garantisce il \glo{successo}.
    Di seguito trovi alcuni esempi di come potrebbe decidere di spendere gli altri \tokensucc{2}.

    \begin{itemize}
        \item Il boia lancia un grido e il cavallo di una delle guardie si imbizzarrisce.
        \item Una torcia lasciata cadere appicca un incendio.
        \item Il malcapitato approfitta della situazione e toglie il cappio dal collo di un secondo condannato.
    \end{itemize}
    }
spendere il primo \tokensucc{} per avere \glo{successo} nella \glo{prova}.
Per ogni \tokensucc{} speso oltre al primo puoi migliorare ulteriormente il risultato che hai ottenuto.

Il modo in cui l'effetto della \glo{prova} viene migliorato è a scelta del \glo{giocatore}; gli altri \glo{giocatori} e il \glo{narratore} possono contribuire suggerendo delle idee o facendo domande.
Ecco una lista di effetti positivi che potrebbero accadere per ogni \tokensucc{} speso oltre al primo:
\begin{itemize}
    \item L'azione ha una conseguenza positiva inaspettata (questo vantaggio deve essere secondario rispetto all'effetto dell'azione).
    \item Il risultato ottenuto durerà più a lungo.
    \item Il risultato ottenuto influenzerà più di un soggetto.
\end{itemize}

Questa lista è indicativa, usa la tua fantasia e mantieniti sui binari di una narrazione coerente.
È responsabilità del \glo{giocatore} fare sì che ciò che viene raccontato sia plausibile e allineato con ciò che accade in \glo{scena}.

\subsection{Rafforzare un \glo{tratto}}
Colloca
\marginpar{Lothar colloca \tokensucc{1} su \emph{Tirare con l'arco}, \glo{tratto} che ha messo in gioco per affrontare la \glo{prova}, feliuce del fatto che la sua mira è ancora ottima.}
\tokensucc{1} su uno dei \glo{tratti} che hai messo in gioco durante la \glo{prova}.
La prossima volta in cui lo metterai in gioco, aggiungerai anche questo \tokensucc{} oltre al primo.
Ogni \glo{tratto} può ospitare al massimo \tokensucc{1} alla volta.

\section{Utilizzare le \glo{complicazioni}}
Devi
\marginpar{Roberto vuole recidere con una freccia la corda legata al collo di un malcapitato che sta per essere impiccato.
    \glo{Estrae} \tokenfail{3}.
    Dovrà decidere come spenderli.}
fare uso di tutti i \tokenfail{} che \glo{estrai}.
Puoi spenderli per \glo{complicare} la \glo{scena}, subire una \glo{sventura} o accumulare \glo{adrenalina} o \glo{confusione}.
Ogni \tokenfail{} può essere speso separatamente.

\subsection{\glo{Complicare} la \glo{scena}}
Consegna
\marginpar{Consegna \tokenfail{1} al \glo{narratore}, chiedendogli di complicare la \glo{scena}.
    La folla strilla quando la freccia si pianta nelle assi del patibolo, ma il boia e le guardie sembrano più in allerta che stsupite.
    Capite tutti cosa sta succedendo: è un'imboscata!}
1 o più \tokenfail{} e chiedi al \glo{narratore} di \glo{complicare} la \glo{scena}.
Spendere \tokenfail{} in questo modo evita conseguenze dirette sul tuo \glo{eroe}, ma complica le cose per tutti.
Per ogni \tokenfail{} speso in questo modo, la \glo{scena} si farà più \glo{pericolosa} o le \glo{prove} al suo interno si faranno più \glo{difficili}.
Il modo in cui la \glo{scena} si \glo{complica} è a scelta del \glo{naratore}, ma se tu o gli altri \glo{giocatori} avete buone idee potete proporle.

\subsection{Subire una \glo{sventura}}
Colloca
\marginpar{Dopodiché, colloca \tokenfail{1} su una \glo{sventura}.
    Un contadino tra la folla indica Lothar e fa ``Ehi, ma quello non è quel cacciatore di taglie?''.
    Le guardie si voltano nella sua direzione.
    Lothar è \emph{Braccato}.

    Nel tentativo di fuggire Lothar combatte due guardie.
    Affronta una \glo{prova} in cui le uccide e in cui spende \tokenfail{2} in \glo{sventure}.
    Il \glo{narratore} descrive che durante il combattimento Lothar rimane ferito e gli impone la \glo{sventura} da \tokenfail{1} \emph{Spalla lussata}.
    Inoltre l'omicidio delle guardie aggrava \emph{Braccato} nella \glo{sventura} da \tokenfail{2} \emph{Nemico pubblico}.}
1 o più \tokenfail{} sulla scheda dell'\glo{eroe}, nella sezione dedicata a ospitare le \glo{sventure}, e chiedi al \glo{narratore} di importene una per ogni \tokenfail{} speso in questo modo.
Le \glo{sventure} sono condizioni negative che affliggono l'\glo{eroe}.
Possono comprendere fattori fisici, psicologici ed emotivi di qualsiasi tipo e sono sempre temporanee,
Il \glo{narratore} ti racconterà cosa accade al tuo \glo{eroe} e ti comunicherà la \glo{sventura}, che dovrai scrivere in uno degli esagoni dedicati.

Se spendi più di \tokenfail{1} per infliggere \glo{sventure} al tuo \glo{eroe}, il \glo{narratore} sceglie come ripartirli in un numero di \glo{sventure} a sua scelta.
Potrebbe infliggerti una sola \glo{sventura} a cui sono associati più \tokenfail{} invece che infliggerti più \glo{sventure} separate.
Inoltre, se l'\glo{eroe} è segnato da \glo{sventure} subite in precedenza, il \glo{narratore} potrebbe decidere di aggravare quelle, aggiungendovi \tokenfail{} ulteriori invece che infliggerne di nuove.
La gravità di una \glo{sventura} in termini narrativi dovrebbe essere allineata con il numero di \tokenfail{} a essa associati.

\subsubsection{Effetto delle sventure}
Ogni
\marginpar{Essere \emph{Braccato} aggiungerà \tokenfail{1} al \glo{sacchetto} di Lothar quando affronta \glo{prove} come nascondersi, sfuggire alle guardie, convincere qualcuno a sviare i suoi inseguitori.}
volta che una \glo{sventura} entra in gioco in una \glo{prova}, aggiungi i \tokenfail{} a essa associati al tuo \glo{sacchetto}.
Una volta effettuata la \glo{prova}, se la \glo{sventura} è ancora presente, posiziona di nuovo i \tokenfail{} nelle sue vicinanze.
Ogni \glo{sventura} dovrebbe condizionare la narrazione e il modo in cui interpreti l'\glo{eroe}.

\subsubsection{Durata delle sventure}
Una
\marginpar{Fuggire dal paese e volare basso per qualche giorno sarebbe una condizione sufficiente a rimuovere la \glo{sventura} \emph{Braccato}.}
\glo{sventura} affligge l'\glo{eroe} finché non vi si pone rimedio o le sue conseguenze si esauriscono.
Deve accadere qualcosa durante la \glo{storia} che giustifichi il fatto che la \glo{sventura} venga rimossa.

\subsection{Accumulare \glo{adrenalina} o \glo{confusione}}
\glo{Adrenalina} e \glo{confusione} sono condizioni che influenzano la capacità del tuo \glo{eroe} di controllarsi nella prossima \glo{prova} che affronterà.
Puoi importele se non vuoi consegnare \tokenfail{} al \glo{narratore}.

\subsubsection{Come funziona l'adrenalina?}
Colloca
\marginpar{Roberto colloca \tokenfail{1} sull'esagono deidcato all'\glo{adrenalina} e descrive come il cuore di Lothar batta all'impazzata.
    Ha solo pochi secondi per salvare la situazione.
    Nela prossima \glo{prova} che affronterà dovrà \glo{estrarre} per forza \tokenrand{4}.}
\tokenfail{1} sull'esagono dedicato all'\glo{adrenalina} se pensi che l'esito della \glo{prova} che hai appena affrontato possa aver \glo{agitato} l'\glo{eroe}.
Nella prossima \glo{prova} sarai costretto a \glo{estrarre} \tokenrand{4}.
Dopo aver \glo{estratto}, rimuovi il \tokenfail{} collocato sull'\glo{adrenalina} dalla scheda.

\subsubsection{Come funziona la confusione?}
Colloca
\marginpar{Roberto colloca \tokenfail{1} sull'esagono dedicato alla \glo{confusione}.
    Lothar non sarà padrone di sé durante la prossima \glo{prova}.
    Se metterà in gioco 4 \glo{tratti}, invece che aggiungere \tokensucc{4} al \glo{sacchetto} aggiungerà \tokenrand{4}, che andarnno inserito casualmente, ad esmpio ad occhi chiuso.}
\tokenfail{1} sull'esagono dedicato alla \glo{confusione} se pensi che l'esito della \glo{prova} abbia fatto sentire smarrito l'\glo{eroe}.
Nella prossima \glo{prova} i \glo{tratti} che metterai in gioco non ti faranno aggiungere al \glo{sacchetto} dei \tokensucc{}, bensì dei \tokenrand{} pescati alla cieca dalla \glo{riserva}.
Aggiungi tanti \tokenrand{} quanti sono i \glo{tratti} che metti in gioco, dopodiché rimuovi il \tokenfail{} collocato sulla \glo{confusione}.
Se un altro \glo{eroe} ti aiuta durante la \glo{prova}, sta a te decidere se il \tokensucc{} che ricevi sia o meno influenzato dalla \glo{confusione}.

\subsection{Quando conviene lasciare \glo{complicazioni} e \glo{sventure} al \glo{narratore}?}
Ogni volta che è divertente.
In \nte{} il \glo{narratore} sfrutta i \tokenfail{} per rispondere alle azioni dei \glo{giocatori}.
Questo significa che i \tokenfail{} sono lo strumento che permette alle \glo{scene} di mantenersi interessanti e alla \glo{storia} di rimanere in moto sui binari a cui i giocatori tengono.

Più i \glo{giocatori} affrontano \glo{prove}, più generano \glo{successi} e \glo{complicazioni}.
Sono le \glo{complicazioni} a dare vita alla \glo{storia}, permettendole di svilupparsi lungo filoni inaspettati.

Una situazione del tutto armonica si risolve in tempi brevi, mentre una complicata è interessante da vivere ed esplorare.
Subire \glo{sventure} e \glo{complicazioni} non è una punizione, ma il modo per vivere avventure memorabili.

In \nte{} sono i \glo{giocatori} a chiedere al \glo{narratore} di creare \glo{complicazioni}, e il suo compito è agire in modo che queste richieste vengano sempre accolte in modo originale e inaspettato.
Tramite questa economia di gioco vi trovere spesso e volentieri a essere più coinvolti e divertiti da \glo{prove} andate molto male che da \glo{prove} andate molto bene.

\section{Spendere \glo{token} con \glo{lezioni} e \glo{cicatrici}}
\marginpar{Lothar conosce la \glo{lezione} \emph{Un ribelle giace nel profondo di ogni anima} e può collocare \tokensucc{1} sulla carta che descrive la \glo{lezione}.
    Potrà spenderlo in seguito per raccontare come la voce della liberazione del malcapitato spingerà una persona o un piccolo gruppo a non piegare la testa e ribellarsi all'ingiustizia.

    Etienne è segnato dalla \glo{cicatrice} \emph{Ho visto mia moglie morire}.
    Quando deciderà di metterla in gioco aggiungerà \tokenfail{1} al \glo{sacchetto}, ma potrà ignorare 1 dei \tokenfail{} che \glo{estrarrà} durante la \glo{prova}.}
Nella \namecref{part:evolvere-eroe} \nameref{part:evolvere-eroe} scoprirai che il tuo \glo{eroe} può apprendere delle \glo{lezioni} nel corso della \glo{storia} o essere segnato da \glo{cicatrici}.

Le \glo{lezioni} permettono al \glo{giocatore} di mettere a frutto \tokensucc{} o \tokenfail{} in modo unico, mentre le \glo{cicatrici} sono esperienze traumatiche che rendono l'\glo{eroe} più coriacei nel tollerare i \tokenfail{} che \glo{estrae}.

\glo{Lezioni} e \glo{cicatrici} sono modalità di evoluzione facoltative, che puoi decidere di utilizzare nella misura che ritieni adeguata per dare spessore e struttura al tuo \glo{eroe}.
