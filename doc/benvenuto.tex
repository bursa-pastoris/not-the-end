% !TEX encoding = utf8
% !TEX program = pdflatex
% !TEX root = ../not-the-end
% !TEX spellcheck = it-IT

\chapter{Benvenuto in \nte{}}

\nte{} è un gioco di ruolo che narra le avventure di eroi disposti a rischiare il tutto per tutto per ciò che considerano importante, e lo fa con un sistema di gioco originale che si chiama \hexsys{}.

In questo gioco, ogni \glo{eroe} è definito tanto dai suoi successi quanto dai suoi fallimenti e ogni esperienza, per quanto traumatica, lo rende più forte e sfaccettato di prima.

L'\glo{eroe} è descritto da parole chiave posizionate in un alveare di esagoni, ciascuno dei quali rappresenta uno dei suoi \glo{tratti} distintivi.
La vicinanza degli esagoni all'interno dell'alveare crea relazioni e legami che rendono ogni \glo{eroe} assolutamente unico.

\hexsys{} non usa dadi per risolvere conflitti e situazioni pericoloe, ma \glo{token} di diverso colore che rappresentano \glo{successi} e \glo{complicazioni}.

Ogni \glo{prova} che affronterai darà vita a risultati che aggiungono qualcosa alla \glo{storia} e al mondo di gioco, rendendo l'esito di ogni \glo{prova} unico e diverso dal precedente.

Sii pronto a rischiare, trionare, fallire e rialzarti.
Questa non è la \glo{fine}!

\section{Cos'è un gioco di ruolo}
In un gioco di ruolo i \glo{giocatori} vestono i panni dei protagonisti di una \glo{storia}, interpretandone i moventi e decidendone le azioni.
Se non ti è mai capitato di giocare di ruolo non preoccuparti: la cosa può sembrare difficile, ma appena ti metterai al tavolo scoprirai che ti viene naturale come narrare un racconto attorno al fuoco.

Nel gruppo ci sarà un \glo{giocatore} con un compito speciale: sarà sua la responsabilità di descrivere il mondo che circonda gli \glo{eroi}, interpretare tutti i \glo{personaggi secondari} e facilitare lo svolgersi del gioco.
D'ora in poi lo chiameremo \glo{narratore}.

Il gioco si svolge come una conversazione tra \glo{narratore} e \glo{giocatori}.
Il \glo{narratore} descrive la scena, i \glo{giocatori} rispondono descrivendo le azioni dei loro \glo{eroi}, la situazione si evolve e il ciclo ricomincia.

A volte il gioco inserirà un elemento di casualità per dare un tocco di imprevedibilità alla storia.
Lo scopo di un gioco di ruolo è quello di farvi divertire vivendo un'avventura assieme: ricordati di lasciare spazio a tutte le persone al tavolo e contribuisci alla storia a modo tuo, senza forzature.

\section{I pilastri di \nte{}}
\nte{} racconta storie intense, che segnano indelebilmente gli \glo{eroi} che le vivono.
Quando giochi, tieni sempre presenti i tre pilastri che sorreggono la narrativa di gioco:
\begin{itemize}
    \item \glo{Rischia} per ciò che consideri importante
    \item Lascia che la \glo{storia} cambi il tuo \glo{eroe}
    \item Vivi la \glo{fine} come un nuovo inizio
\end{itemize}

Nei prossimi paragrafi troverai un approfondimento per ognuno dei tre pilastri e un riferimento alle regole di gioco che li supportano.

\subsection{Rischia per ciò che consideri importante}
\nte{} mette il \glo{giocatore} in condizioni di esporre il proprio \glo{eroe} a situazioni rischiose e problematiche, senza per questo sentirsi punito.
Le \glo{complicazioni} vissute dall'\glo{eroe} sono parte integrante dell'esperienza di gioco e vanno vissute come un'opportunità per esplorarne la personalità, i moventi e i valori.

Leggi come \glo{affrontare le prove} a \cpageref{chap:affrontare-prove} e scopri come \glo{rischiare} a \cpageref{sec:decidere-rischiare}.

\subsection{Lascia che la storia cambi il tuo eroe}
Gli \glo{eroi} di \nte{} evolvono quando vivono sulla loro pelle le conseguenze delle proprie azioni.
Il \glo{giocatore} potrà decidere quali sono le \glo{prove cruciali} per l'evoluzione del suo \glo{eroe}, ma dovrà deciderlo prima di conoscerne l'esito.
In questo modo ogni \glo{eroe} di \nte{} evolverà in base alle esperienze significative che vivrà durante la \glo{storia}.

Consulta la \namecref{part:evolvere-eroe} \nameref{part:evolvere-eroe} a \cpageref{part:evolvere-eroe} per saperne di più.

\subsection{Vivi ogni fine come un nuovo inizio}
Ogni \glo{eroe} di \nte{}, durante il corso della storia, si trova faccia a faccia con la \glo{fine}.
A volte è la sua \glo{fine}, a volte è la \glo{fine} di qualcosa di importante per lui, come un viaggio, un'amicizia, un giuramento, un antico rancore.
\nte{} mette in luce questi momenti permettendo al giocatore di far evolvere l'\glo{eroe} in modo unico.

Consulta il \namecref{sec:farsi-segnare-cicatrice} dedicato alle \glo{cicatrici} a \cpageref{sec:farsi-segnare-cicatrice} e scopri come \glo{affrontare la fine} a \cpageref{part:affrontare-fine}.

\section{Dov'è ambientato il gioco}
Le \glo{storie} di \nte{} possono essere ambientate ovunque: in reami lontani popolati da draghi e potenti stregoni, in distopie futuristiche dominate da spietate intelligenze artificiali, nel buio e infinito spazio profondo, nel mondo reale o in qualunque altro contesto tu conosca o possa immaginare.

\subsection{Come scegliere l'ambientazione}
Giocare
\marginpar{``Tanto tempo fa, in una galassia lontanta lontana'', ``Tra le vie polverose di Redwater's Creek, nell'America dei pionieri'' o ``In una colonia spaziale immersa tra i ghiacci eterni di Ganimede, il 15 Dicembre 2047'', sono tutti ottimi esempi di \emph{dove} e \emph{quando}.}
a \nte{} significa innanzitutto scegliere un \emph{dove} e un \emph{quando}, ovvero un luogo e un tempo in cui ambientare la \glo{storia}.


Per cominciare non è necessario andare troppo nello specifico, ciò che serve è il minimo indispensabile per avere un terreno comune sul quale immaginare.

Questa è una scelta comune che tutto il gruppo di gioco dovrebbe fare insieme.
Sebbene delegare al \glo{narratore} la scelta dell'\glo{ambientazione} sia una possibilità, è molto più divertente vivere una \glo{storia} ambientata in un contesto che tutti i \glo{giocatori} conoscono, amano o desiderano vivere e esplorare.

Per cominciare non serve nulla più che una frase, ma sentitevi liberi di aggiungere qualche dettaglio se lo ritenete necessario o di valore.

Una volta scelto un \emph{dove} e un \emph{quando}, creare gli \glo{eroi} ti condurrà in modo naturale a intesserli nel contesto di gioco: questo genererà gli agganci narrativi necessari ai \glo{giocatori} per avere \glo{eroi} ben inseriti nella storia fin dall’inizio.

Proprio come per la scelta dell'\glo{ambientazione}, creare gli \glo{eroi} assieme è più efficace e divertente.
Vi permetterà di accordarvi sulle loro relazioni e darà al \glo{narratore} spunti utili ad avviare la \glo{storia}.

La \namecref{part:ambientazioni} \nameref{part:ambientazioni} è dedicata a darti approfondimenti su come creare contesti in cui ambientare le tue storie con il minor sforzo possibie e la \namecref{part:esempi-ambientazione} \nameref{part:esempi-ambientazione} fornisce cinque esempi da prendere a riferimento o in cui giocare.

\section{Il sistema di gioco}
\nte{} utilizza \hexsys{}, un sistema digioco originale che utilizza \glo{token} di diverso colore, che vengono inseriti e pescati alla cieca da un \glo{sacchetto} per risolvere le situazioni importanti durante la \glo{storia}.

\subsubsection{Cosa serve per giocare}
Per giocare ti serviranno:
\begin{itemize}
    \item una ventina di \glo{token} di due colori diversi, come delle pedine della dama, delle fiches da poker o delle pietre da go, che non si possano distinguere al tocco;
    \item un \glo{sacchetto} opaco in cui inserire e da cui pescare i \glo{token}, abbastanza grande da infilarci un mano;
    \item una scheda dell'\glo{eroe} per ogni \glo{giocatore};
    \item qualche gomma e qualche matita.
\end{itemize}

\section{I simboli di gioco}
I \glo{token} di colori diversi che vengono usati in \nte{} rappresentano \glo{successi} e \glo{complicazioni}.
Da questo momento in poi i \glo{token} che indicano un \glo{successo} saranno segnalati con il simbolo \tokensucc{}, quelli che indicano una \glo{complicazione} con il simbolo \tokenfail{}.
I \glo{token} che vanno \glo{estratti} o scelti casualmente saranno indicati dal simbolo \tokenrand{}.
Utilizzi i \glo{token} ogni volta che il tuo \glo{eroe} affronta una \glo{prova}.

\section{I termini di gioco}
\begin{itemize}
    \item \emph{Cicatrice}: una
        \marginpar{\emph{Solo al mondo}, \emph{Disilluso}, \emph{Antieroe}, \emph{Esperimento di laboratorio}, \emph{Orfano}, \emph{Istinto di morte}.}
        parola chiave o una piccola frase che descrive un trauma che ha segnato l'\glo{eroe}.
        Ogni \glo{cicatrice} è inscritta in un esagono della scheda dell'\glo{eroe}.

    \item \emph{Complicazione}: ciò che di negativo accade a valle della \glo{prova}, che equivale ai \tokenfail{} che il \glo{giocatore} ha \glo{estratto} dal \glo{sacchetto}.

    \item \emph{Difficoltà}: valore tra 1 e 6 che indica quanto è probabile che la \glo{prova} crei \glo{complicazioni}.

    \item \emph{Eroe}: uno dei protagonisti della \glo{storia}.

    \item \emph{Estrarre}: l'azione di pescare alla cieca da 1 a 4 \tokenrand{} dal \glo{sacchetto}.

    \item \emph{Giocatore}: chi nel gruppo di gioco interpreta un \glo{eroe}.

    \item \emph{Lezione}: qualcosa
        \marginpar{\emph{La fortuna aiuta gli audaci}, \emph{L'onore è sacrificio}, \emph{Sangue chiama sangue}, \emph{Conosco il mio destino}.}
        che l'\glo{eroe} ha imparato sulla sua pelle, che gli permette di utilizzare \glo{successi} e \glo{complicazioni} in modo unico.

    \item \emph{Narratore}: chi nel gruppo di gioco interpreta i \glo{personaggi secondari}, descrive le \glo{sfide}, e racconta le \glo{complicazioni} che accadono durante la \glo{storia}.

    \item \emph{Obiettivo}: risultato
        \marginpar{Vincere un duello di spada, superare un firewall, intrufolarsi non visti in un edificio.}
        che gli \glo{eroi} desiderano ottenere, collegato a una \glo{sfida}, una \glo{scena} o una situazione di gioco.

    \item \emph{Personaggio secondario}: personaggio della \glo{storia} che non sia uno degli \glo{eroi}.

    \item \emph{Pericolo}: valore tra 1 e 4 che indica quanto è probabile che l'\glo{eroe} \glo{esca di scena}.

    \item \emph{Profondità}: numero di \glo{successi} necessari a raggiungere un \glo{obiettivo}.

    \item \emph{Prova}: l'atto
        \marginpar{Convincere uno sconosciuto ad aiutarti, mettere fuori combattimento una guardia di ronda.}
        di \glo{estrarre} dei \tokenrand{} da un \glo{sacchetto} per verificare l'esito di un'azione rilevante per la \glo{storia} e che potrebbe generare delle \glo{complicazioni}.

    \item \emph{Rischiare}: la scelta di mettere l'\glo{eroe} in pericolo per estrarre altri \tokenrand{}.

    \item \emph{Riserva}: un
        \marginpar{Un astuccio, una ciotola o semplicemente il centro del tavolo.}
        luogo o un contenitore comodamente accessibile a tutti i \glo{giocatori} da cui prendere e in cui scartare i \tokenrand{}.

    \item \emph{Sacchetto}: un contenitore opaco in cui inserire ed estrarre i \tokenrand{} necessari per affrontare una \glo{prova}.

    \item \emph{Scena}: una
        \marginpar{Uno scontro, un viaggio, un'indagine.}
        porzione della \glo{storia} in cui si esaurisce un singolo momento di gioco.

    \item \emph{Sessione}: quando ti trovi a giocare con i tuoi amici, giochi una \glo{sessione}.

    \item \emph{Sfida}: soggetto
        \marginpar{Uno spadaccino esperto, un canyon pericoloso.}
        o luogo con cui gli \glo{eroi} devono controntarsi.

    \item \emph{Storia}: un
        \marginpar{Rovesciare un regime, recuperare un artefatto.}
        intero arco narrativo, caratterizzato da un tema o un obiettivo.

    \item \emph{Successo}: ciò che di buono accade a valle della \glo{prova}, che equivale ai \tokensucc{} che il \glo{giocatore} ha \glo{estratto} dal \glo{sacchetto}.

    \item \emph{Token}: gettoni
        \marginpar{Potete usare qualsiasi cosa come \tokenrand{} finché sono oggetti che potete inserire nel \glo{sacchetto}, basta siano di due colori diversi e identici al tatto.}
        di due colori diversi indistinguibili al tatto, necessari ad affrontare le \glo{prove}.
        Nel manuale sono indicati con il simbolo \tokenrand{} queli \glo{estratti} dal \glo{sacchetto}, con il simoblo \tokensucc{} quelli che indicano un successo e con il simbolo \tokenfail{} quelli che indicano un fallimento.

    \item \emph{Tratto}: una
        \marginpar{\emph{Hacker}, \emph{Samurai}, \emph{Segugio}, \emph{Drago}, \emph{Scaltro}, \emph{Possente}, \emph{Audace}, \emph{Paranoico}, \emph{Lestofante}.}
        parola chiave o una piccola frase che descrive un aspetto importante dell'\glo{eroe}.
        Ogni \glo{tratto} è inscritto in un esagono della scheda dell'\glo{eroe}.

    \item \emph{Turno}: quando la situazione si fa concitata si agisce a \glo{turno}.
        Un \glo{turno} è passato quando tutti i \glo{giocatori} hanno agito.

    \item \emph{Uscire di scena}: condizione del'\glo{eroe} che gli impedisce di agire in \glo{scena}.
\end{itemize}

\section{Cos'è una \glo{prova} e quali sono i passi per affrontarla}
Una \glo{prova} è un momento di svolta per una \glo{storia}, in cui il \glo{giocatore} fa agire il suo \glo{eroe} per ottenre un risultato.
Ogni \glo{prova} corrisponde a un'opportunità di esplorare i moventi dell'\glo{eroe} e regalare al \glo{narratore} spunti per movimentare la \glo{storia}.
È necessario affrontare una \glo{prova} quando l'esito dell'azione è incerto ed è possibile che si crenino \glo{complicazioni}.

Questo schema riassuntivo ti sarà utile a comprendere più facilmente le informazioni che troverai in seguito nel manuale.
La \crefname{part:giocare-eroe} \nameref{part:giocare-eroe} illustra nel dettaglio come affrontare le \glo{prove} e la \crefname{part:narrare-storie} \nameref{part:narrare-storie} ti sarà utile a capire come sfruttarle per creare \glo{complicazioni interessanti}.

\begin{center}
    \begin{minipage}[t][][t]{.45\columnwidth}
        \paragraph{Descrivi la tua azione}
        Descrivi l'intenzione dell'\glo{eroe} in modo sintetico.
        Dichiarare qual è lo scopo dell'\glo{eroe} è sempre buona norma, ma non ci sono formule rigide da seguire.
        Sentiti libero di descrivere la tua azione come ti viene naturale.
        Ciò che descrivi determina la \glo{difficoltà} della \glo{prova} e i \glo{tratti} che puoi mettere in gioco.
    \end{minipage}
    \hspace*{\fill}
    \begin{minipage}[t][][t]{.45\columnwidth}
        \paragraph{Componi il sacchetto}
        Aggiungi al \glo{sacchetto} \tokensucc{1} per ogni \glo{tratto} dell'\glo{eroe} che desideri mettere in gioco nella \glo{prova} che stai affrontando.
        La scelta su quali \glo{tratti} mettere in gioco sta a te.
        Dopodiché aggiungi al \glo{sacchetto} un numero di \tokenfail{} pari alla \glo{dififcoltà} della \glo{prova}, che ti verrà comunicata dal \glo{narratore}.
    \end{minipage}

    \bigskip

    \begin{minipage}[t][][t]{.45\columnwidth}
        \marginparfontsize
        Roberto interpreta Lothar, un cacciatore di taglie.
        Lothar è acquattato tra dei cespugli, nelle vicinanze di un accampamento di briganti.
        Roberto dichiara di voler scagliare una freccia verso la guardia di ronda, in modo da ucciderla silenziosamente e avvicinarsi ai margini dell'accampamento senza essere individuato.
    \end{minipage}
    \hspace*{\fill}
    \begin{minipage}[t][][t]{.45\columnwidth}
        \marginparfontsize
        I \glo{tratti} che aiutano Lothar sono \emph{Cacciatore di Taglie}, \emph{Silenzioso}, \emph{Assassinare} è \emph{Infiltrarsi}.
        Il \glo{narratore} sancisce che la \glo{difficoltà} della \glo{prova} è \emph{normale}.
        Roberto aggiunge \tokensucc{4} e \tokenfail{3} nel \glo{sacchetto}.
    \end{minipage}

    \vspace{2\bigskipamount}

    \begin{minipage}[t][][t]{.45\columnwidth}
        \paragraph{Estrai}
        \glo{Estrai} da 1 a 4 \tokenrand{} dal \glo{sacchetto}.
        Più \tokenrand{} decidi di \glo{estrarre}, maggiori saranno i rischi e le probabilità di riuscire.
        Ogni \tokensucc{} \glo{estratto} rappresenta un \glo{successo}, ogni \tokenfail{} una \glo{complicazione}.
        I \tokenrand{} di colori opposti non si annullano tra loro ma, una volta \glo{estratti}, contribuiranno tutti all'esito della \glo{prova}.
    \end{minipage}
    \hspace*{\fill}
    \begin{minipage}[t][][t]{.45\columnwidth}
        \paragraph{Descrivi i risultati}
        Spendi i \tokensucc{} che hai \glo{estratto} per raccontare gli esiti positivi della \glo{prova} e spendi i \tokenfail{} che hai \glo{estratto} per chiedere al \glo{narratore} di inventare \glo{complicazioni} che movimentino la \glo{scena} o di imporre \glo{sventure} all'\glo{eroe}.
    \end{minipage}

    \bigskip

    \begin{minipage}[t][][t]{.45\columnwidth}
        \marginparfontsize
        Roberto decide di pescare 3 \tokenrand{} in tutto, ed \glo{estrae} 2 \tokensucc{} e 1 \tokenfail{}.
        Questo significa che Lothar ha avuto due \glo{successi}, ma che la sua azione ha causato anche una \glo{complicazione}.
    \end{minipage}
    \hspace*{\fill}
    \begin{minipage}[t][][t]{.45\columnwidth}
        \marginparfontsize
        Il primo \tokensucc{} \glo{estratto} assicura a Lothar di riuscire nella \glo{prova}.
        Roberto spende il secondo \tokensucc{} per migliorare l'esito della \glo{prova} e spende il \tokenfail{} chiedendo al \glo{narratore} di imporgli una \glo{sventura}.
        La freccia di Lothar si conficca nella gola della guardia, che cade a terra senza suono in un'area poco visibile dell'accampamento.
        Tuttavia la corda del suo arco si spezza: Lothar si trova \emph{Disarmato}.
    \end{minipage}
\end{center}
