% !TEX encoding = utf8
% !TEX program = pdflatex
% !TEX root = ../not-the-end
% !TEX spellcheck = it-IT

\chapter{La tavola rotonda}
Non esiste al mondo un fantasy più classico del ciclo arturiano: nobili cavalieri, draghi, spade magiche e tradimenti per amore.
Luca ha letto tantissimi libri sulla storia di Re Artù, \emph{Excalibur} è uno dei suoi film preferiti e chiede al resto del gruppo di provare a giocare i cavalieri della Tavola Rotonda.

Il ciclo arturiano che tutti conosciamo è molto lontano dall'Inghilterra del~V secolo e Luca infatti si immagina cavalieri in scintillanti armature complete, che duellano con onore per il proprio re, in una commistione di misticismo e religione che permette di far convivere stregoni come Merlino e Morgana con il Graal.

Quello di Camelot non è un mondo realistico e coerente, ma poetico e fantastico, e ogni \glo{Eroe} potrebbe incarnare un valore cavalleresco, essere un incantatore in grado di cambiare forma, o ancora essere discendente di un re inglese che ha giurato fedeltà a Camelot e Artù, ultimo baluardo contro gli invasori del continente.

Luca chiede ai suoi \glo{giocatori}: ``Tra Artù, Ginevra, Morgana, Lancillotto, Tristano, Mordred\textellipsis{} di tutti questi personaggi iconici, di chi vi piacerebbe vestire i panni in una \glo{storia} fatta di giuramenti spezzati e lotte fratricide, di magia e onore?''.

\section{Fantasia d'altri tempi}
Il ciclo arturiano, pur essendo uno dei fantasy più classici, è molto lontano da quelle storie che hanno reso famoso il genere ai giorni nostri.

La storia di Camelot per come la conosciamo è più vicina al lavoro di un bardo che che a quello di uno storiografo, priva di una linea temporale coerente e ricca di dettagli a volte contraddittori.
Ci sono però alcuni punti fissi e Luca e gli altri decidono di partire da quelli per costruire i propri personaggi e definire la loro Camelot.

Gianni sceglie di interpretare un giovane Artù, che ha da poco estratto Excalibur dalla roccia e che ancora fatica a comprendere i consigli e gli insegnamenti del suo tutore Merlino.
Figlio di Uther Pendragon, è diventato re di Britannia e ora si trova a dover raccogliere il proprio esercito per respingere gli invasori sassoni e fondare l'Impero di Britannia.
Il suo destino è di essere tradito per poi essere poi seppellito ad Avalon.

Federica coglie l'occasione per interpretare Merlino, lo stregone mentore e consigliere di Artù.
Il potere magico di Merlino viene dal suo essere figlio di un demone e di una donna, cosa che gli ha permesso di entrare in comunione con il Respiro del Drago, una forza che permea tutto il mondo e che permette a Merlino di sovvertire le leggi della fisica.
Merlino è legato a Nimue, la Dama del Lago, ed è un personaggio freddo, calcolatore e disposto a sacrificare i propri amici per i propri obiettivi.

Marco sceglie invece Lancillotto, cresciuto dalla stessa Nimue e al contempo miglior amico di Artù nonché suo traditore.
Lancillotto è un cavaliere impareggiabile, coraggioso e astuto, che ha commesso il grave errore di innamorarsi di Ginevra, promessa sposa di Artù.
Lancillotto ha giurato di seguire Artù fino alla morte, ma il suo amore per Ginevra è come una spada di Damocle che pende sopra la sua testa, pronto a incrinare il rapporto tra i due compagni al minimo passo falso.

A opporsi ad Artù e ai suoi cavalieri sarà Mordred, cugino di Artù e legittimo erede al trono di Britannia, aiutato da Morgana, sorellastra di Artù e allieva di Merlino.
In un mondo vasto come quello della Britannia mitica di Artù i pericoli giungono sempre da chi ti è più vicino, aggiungendo alle rivalità il coinvolgimento personale.

\section{Stregoni, draghi, cavalieri e spade magiche}
Nelle storie del ciclo arturiano la magia è un dato di fatto, così come lo è per noi sapere che la Terra gira intorno al Sole: qualcuno potrebbe non crederci, ma è una realtà che la maggior parte delle persone non si sognerebbe di mettere in discussione.
Questo non vuol dire che la magia sia a portata di tutti o che sia ben vista da tutti: la magia è una forza arcaica, con cui solo druidi e stregoni osano immischiarsi sfidando il volere di Dio.
Anche se nessuno dei cavalieri oserebbe mai cercare di usare incantesimi e sortilegi, nessuno di loro si è mai opposto all'aiuto di Merlino, né tantomeno si è tirato indietro davanti a oggetti magici: Artù porta al suo fianco Excalibur e il suo fodero, che lo protegge dalle ferite mortali; Lancillotto ha ricevuto in dono da Nimue, la Dama del Lago, che l'ha cresciuto, un anello in grado di svelare ogni menzogna e ogni illusione; Bedivere impugna una lancia in grado di uccidere i draghi; Parsifal parte alla ricerca del santo Graal, la coppa che ha raccolto il sangue di Cristo e che è in grado di curare qualsiasi male.

In un modo o nell'altro tutti i personaggi si ritrovano ad avere un legame con la magia: Gianni segna come \glo{tratto} per Artù \emph{Excalibur}, perché è un oggetto così iconico da rappresentare Artù quanto il suo essere un re.
In tutti i racconti che parlano di Artù, il re bretone finisce sempre per scegliere la spada rispetto al fodero, cosa che, alla fine, lo condanna a morte durante il suo ultimo scontro con Mordred.

Marco invece decide che l'anello di Lancillotto non è così importante da diventare un tratto e lo segna quindi tra le \glo{risorse}, ma tra i \glo{tratti} segna comunque \emph{Figlio di una fata}, per ricordare a tutto il gruppo di essere stato cresciuto da Nimue.

Federica segna come archetipo di Merlino \emph{Stregone} e inserisce la qualità \emph{Respito del Drago}, a cui collega anche l'abilità \glo{Conoscenze proibite}.
Tra queste, Federica immagina che ci siano anche conoscenze del mondo fatato e delle antiche creature che abitano il mondo oltre il velo dell'illusione: giganti, folletti, draghi e alberi dotati di volontà costellano i racconti del ciclo arturiano e sono in genere antagonisti che i cavalieri devono battere grazie al loro coraggio o alla loro astuzia.

\section{Tratti suggeriti}
\begin{center}
    Pensare ai \glo{tratti} di un personaggio del ciclo arturiano è facile, basta leggere un riassunto delle leggende sul singolo cavaliere che si vuole interpretare e gli epiteti con cui gli autori hanno descritto i personaggi: Lancillotto sarà sicuramente un valoroso cavaliere che finirà per tradire il proprio sire, Galahad veniva chiamato il puro, Parsifal è il cercatore del Graal e ovviamente Merlino è un potente stregone.
\end{center}

\begin{center}
    \begin{minipage}[t][][t]{.3\columnwidth}\centering
        \textsc{Archetipi}
        \medskip\\
        Re di Britannia\\
        Stregone\\
        Cavaliere\\
        Fata\\
        Giullare\\
        Cercatore\\
        Guardiano\\
        Cacciatore di mostri\\
        Campione\\
        Cantastorie
    \end{minipage}
    \hspace{\fill}
    \begin{minipage}[t][][t]{.3\columnwidth}\centering
        \textsc{Qualità}
        \medskip\\
        Nobile\\
        Tradutore\\
        Puro\\
        Coraggioso\\
        Onorevole\\
        Spadaccino\\
        Stratega\\
        Onesto\\
        Valoroso\\
        Fedele
    \end{minipage}
    \hspace{\fill}
    \begin{minipage}[t][][t]{.3\columnwidth}\centering
        \textsc{Abilità}
        \medskip\\
        Guidare gli altri\\
        Conoscenze proibite\\
        Ingannare\\
        Duellare\\
        Mantenere la parola\\
        Trovare tracce\\
        Profetizzare\\
        Mutare forma\\
        Resistere alla tentazione\\
        Combattere
    \end{minipage}
\end{center}

\begin{center}
    \textsc{Tratti alternativi}

    Excalibur, Figlio di una fata, Respiro del Drago, Pendragon, Uno contro cento
\end{center}

\section{Risorse suggerite}
\begin{center}
    Il lato fiabesco del ciclo arturiano rende difficile capire quali siano le \glo{risorse} comuni a Camelot, perché i personaggi fanno tutti parte della corte di re Artù e per loro quello che in altri contesi fantasy dovrebbe essere comprato con centinaia di monete d'oro viene dato per scontato.
    Alcuni oggetti dotati di proprietà magiche potrebbero essere \glo{risorse} rare e solo oggetti unici come il Graal o Excalibur dovrebbero essere \glo{risorse} speciali.
\end{center}

\begin{center}
    \begin{minipage}[t][][t]{.3\columnwidth}\centering
        \textsc{Comuni}
        \medskip\\
        Spada\\
        Armatura\\
        Cavallo da guerra\\
        Scudo\\
        Lancia
    \end{minipage}
    \hspace{\fill}
    \begin{minipage}[t][][t]{.3\columnwidth}\centering
        \textsc{Rare}
        \medskip\\
        Sfera di cristallo\\
        Castello\\
        Pietre fatate\\
        Scacchiera\\
        Stendardo reale\\
        Pegno di una dama
    \end{minipage}
    \hspace{\fill}
    \begin{minipage}[t][][t]{.3\columnwidth}\centering
        \textsc{Eccezionali}
        \medskip\\
        Excalibur\\
        Anello di Nimue\\
        Sangue di drago\\
        Graal\\
        Ciocca di una fata
    \end{minipage}
\end{center}

\section{Nomi tipici}
\begin{center}
    Artù, Bedivere, Bors, Ettore, Gaheris, Galahad, Gawain, Ginevra, Isotta, Kay, Lamorak, Lancillotto, Merlino, Mordred, Morgana, Pellinore, Parsifal, Tristano, Uther, Ygraine
\end{center}

\section{Mostri ed eserciti}
Gli \glo{eroi} del ciclo arturiano difficilmente restano a Camelot a lungo: la loro è una vita fatta di cerche, che li portano a viaggiare per tutta la Britannia a caccia di mostri, o a combattere contro eserciti invasori, in genere di popolazioni barbare come i Sassoni o pagane come i Romani.

Queste cerche mettono spesso i cavalieri davanti a ostacoli che possono essere superati solo con astuzia, coraggio e costanza, e i nostri \glo{eroi} si troveranno spesso anche ad affrontare scelte morali complicate: cosa scegliere tra il guadagno personale e il bene comune, o tra l'orgoglio personale e l'onore cavalleresco?

Creature come il Cavaliere Verde, che disturba la quiete di Camelot proprio durante i festeggiamenti del Natale e che apparentemente non può essere sconfitto, druidi o draghi mutaforma che vogliono che i propri segreti rimangano tali o esseri fatati che chiedono un sacrificio personale di uno o più cavalieri sono all'ordine del giorno.

Le due più grandi minacce alla pace della Britannia sono Mordred e Morgana, che odiano Artù così tanto da essere pronti a radere al suolo Camelot, e qualunque alleato del re, con un esercito bardato di armature nere e reso invincibile dagli incantesimi della strega.

Ogni sfida, a Camelot, è una questione personale per qualcuno, così come nei racconti del ciclo arturiano c'è sempre un cavaliere che si trova al centro di un conflitto che arriva dal suo passato e che è stato profetizzato nel suo futuro.

\section{Altri elementi suggeriti}
\subsection{Il Cavaliere Verde}
Quale dei cavalieri di Camelot è abbastanza coraggioso da raccogliere la mia sfida?
Sono disposto a lasciare che uno di voi mi colpisca per decapitarmi, a patto poi io possa fare lo stesso con lui.
Accettate il mio duello, o i cavalieri della Tavola Rotonda sono solo dei codardi?

\subsubsection{\glo{Difficoltà} e \glo{pericolo} delle \glo{prove}}
\begin{itemize}
    \item Accettare  e vincere il duello con il Cavaliere Verde è \emph{facile} (\tokenfail{2}), ma \emph{molto pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{2}).
    \item Uccidere definitivamente il Cavaliere Verde usando la propria forza è \emph{impossibile}.
    \item Affrontare il Cavaliere Verde in gruppo è disonorevole e aumenta la \glo{difficoltà} di \tokenfail{1} per ogni persona che partecipa al combattimento.
\end{itemize}

\subsubsection{Possibili \glo{sventure}}
\begin{itemize}
    \item \emph{Codardo}
    \item \emph{Disarmato}
    \item \emph{Alle strette}
\end{itemize}

\subsubsection{Possibili \glo{complicazioni}}
\begin{itemize}
    \item Il Cavaliere Verde si divide in due in seguito a un colpo e affrontarlo diventa \emph{pericolosissimo} (\glo{esci di scena} \glo{estraendo} \tokenfail{1}).
    \item Gli \glo{eroi} riconoscono la magia di Morgana intrisa nella lama dell'ascia del Cavaliere: la strega sta aspettando il momento giusto per colpire Camelot!
\end{itemize}

\subsubsection{Approfondimenti}
Il Cavaliere Verde è una creatura immortale che abita il castello nei pressi di \foreignlanguage{english}{Green Chapel}.
Si presenta a Camelot chiedendo di sfidare a duello il cavaliere più valoroso della Tavola Rotonda.
Il patto è semplice: il cavaliere di Camelot potrà sferrare il primo colpo, e se il Cavaliere Verde riuscisse a sopravvivere avrà diritto a sferrare un colpo a sua volta.
Se la sfida venisse rifiutata, minaccia di riversare propria ira su Camelot e gli abitanti della Britannia.
Il Cavaliere è in realtà un alleato di Morgana e di Mordred, mandato a ridicolizzare Artù e intaccare il morale dei suoi soldati.

\subsection{La corte di Oberon}
Il re delle fate sorride a voi cavalieri, indicando il ricco banchetto che avete davanti a voi.
``Venite, la mia mensa è aperta a tutti, il cibo è caldo e il vino è buono!'' esclama, ma mentre vi guardate intorno e osservate i commensali che mangiano felici, sentite un brivido corrervi lungo la schiena, come se qualcosa di orribile stesse per accadere.

\subsubsection{\glo{Difficoltà} e \glo{pericolo} delle \glo{prove}}
\begin{itemize}
    \item Resistere agli ordini di Oberon dopo aver mangiato o bevuto qualsiasi cosa dalla sua mensa è \emph{quasi impossibile} (\tokenfail{6}).
    \item Percepire ciò che le illusioni di Oberon nascondono è \emph{estremamente difficile} (\tokenfail{5}).
    \item Ferire Oberon è \emph{facilissimo} (\tokenfail{1}) e \emph{pericolosissimo} (\glo{esci di scena} \glo{estraendo} \tokenfail{1}) a causa degli incantesimi che lo proteggono.
\end{itemize}

\subsubsection{Possibili \glo{sventure}}
\begin{itemize}
    \item \emph{Ammaliato}
    \item \emph{Allucinazioni}
    \item \emph{Avvelenato}
\end{itemize}

\subsubsection{Possibili \glo{complicazioni}}
\begin{itemize}
    \item Venite separati da un incantesimo: ognuno di voi si trova da solo in una delle stanze del palazzo di Oberon!
    \item Oberon vi propone un patto: libererà tutte le persone prigioniere della sua corte se un cavaliere si offrirà di prestare giuramento e diventare una delle sue guardie, ripudiando Artù.
\end{itemize}

\subsubsection{Approfondimenti}
Oberon, il re delle fate, è una creatura lasciva e interessata solo a divertirsi alle spalle degli umani.
La sua corte, così confortevole e allettante, è solo l'ennesima trappola in cui Oberon si diletta ad attirare i viandanti, offrendo loro un pasto caldo e un letto per dormire.
Peccato che chiunque prenda posto alla corte non se ne possa più andare, finendo per dimenticarsi di ciò che c'è fuori dalla corte.

\subsection{Mordred e Morgana}
L'esercito di Mordred ha ormai circondato tutta Camelot e guardando dalla torre più alta del castello riuscite a vedere Mordred, nella sua armatura dorata, a cavallo in testa al suo esercito.
Al suo fianco vedete Morgana, circondata dalla nebbia magica del Respiro del Drago.

\subsubsection{\glo{Difficoltà} e \glo{pericolo} delle \glo{prove}}
\begin{itemize}
    \item Raggiungere Mordred evitando di essere sopraffatti dall'esercito è \emph{difficilissimo} (\tokenfail{5}) e \emph{abbastanza pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{3}).
    \item Affrontare Mordred a duello è \emph{normale} (\tokenfail{3}) e \emph{molto pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{2}).
    \item Affrontare Morgana senza l'aiuto di uno stregone è \emph{molto difficile} (\tokenfail{4}) e \emph{molto pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{2}).
\end{itemize}

\subsubsection{Possibili \glo{sventure}}
\begin{itemize}
    \item \emph{Circondato}
    \item \emph{Ferito}
    \item \emph{Ustionato}
\end{itemize}

\subsubsection{Possibili \glo{complicazioni}}
\begin{itemize}
    \item Mordred svela il doppio gioco di uno dei cavalieri della Tavola Rotonda.
    \item Morgana crea un muro di fuoco che vi impedisce di ritirarvi.
    \item Morgana usa il Respiro del Drago per tramutare Mordred in un drago sputafuoco, \emph{molto difficile} (\tokenfail{4}) e \emph{molto pericoloso} (\glo{esci di scena} \glo{estraendo} \tokenfail{2}) da affrontare.
\end{itemize}

\subsubsection{Approfondimenti}
Mordred odia Artù con tutto il suo cuore e brama la vendetta su Camelot più di ogni cosa.
Proprio Mordred convince Lancillotto a farsi avanti con Ginevra per poi rivelare il tradimento ad Artù.
Sempre Mordred convince Artù a una tregua per cacciare un gigantesco serpente che minaccia la Britannia, solo per rivelare che il serpente non esiste e cercando subito dopo di pugnalare Artù a tradimento.
Mordred riesce sempre  a fuggire grazie all'aiuto di Morgana, la sorellastra di Artù, che ritiene il re un presagio di sventura per la Britannia.

\subsection{Di cavalieri, armi e onori}
Nel ciclo arturiano, se si fa eccezione per pochi personaggi, tutti i protagonisti delle leggende sono cavalieri.
Anche quelli che a prima vista sembrano semplici popolani si rivelano sempre figli segreti di qualche nobile, discendenti di qualche fata o più semplicemente valorosi combattenti il cui valore permette loro di guadagnare un posto alla Tavola Rotonda.

Questo non significa però che tutti gli eroi che giocherete debbano essere valenti guerrieri votati al codice della cavalleria: Merlino e Morgana sono solo due degli stregoni che animano le storie di Camelot.
Ginevra, pur non imbracciando mai le armi, è una figura estremamente importante, che guida e sostiene la gente di Camelot e Artù stesso.

Inoltre ci sono scudieri, re di paesi vicini e lontani che si trovano a passare qualche tempo a Camelot e creature divise tra due mondi come Nimue o altri abitanti di Avalon, Tintagel e boschi limitrofi.

Potrebbe anche essere interessante esplorare la vita nel castello proprio dal punto di vista di quei personaggi che si trovano sempre al margine delle storie dei cavalieri: come si comportano i servi del re quando lui e i suoi cavalieri sono in guerra?
Merlino si lascia andare, rinunciando al suo fare impettito e severo, quando l'unica persona nella stanza è un ciabattino?
E quali segreti si lasciano scappare i nobili quando pensano di non essere ascoltati?

Ancora più interessante potrebbe essere una partita in cui gli eroi sono quei mostri che i cavalieri hanno giurato di sconfiggere durante le loro cerche: giganti, folletti, draghi mutaforma e fate potrebbero avere tutto l'interesse di vedere Camelot cadere, aiutando Mordred o Morgana invece che Artù.

\begin{figure}[tbh]
    \centering
    \includesvg{img/artu}
    \caption{Artù}
\end{figure}

\begin{figure}[tbh]
    \centering
    \includesvg{img/merlino}
    \caption{Merlino}
\end{figure}

\begin{figure}[tbh]
    \centering
    \includesvg{img/lancillotto}
    \caption{Lancillotto}
\end{figure}