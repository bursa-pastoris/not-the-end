% !TEX encoding = utf8
% !TEX program = pdflatex
% !TEX root = ../not-the-end
% !TEX spellcheck = it-IT

\chapter{Come far evolvere l'\glo{eroe}}
Gli \glo{eroi} di \nte{} evolvono quando vivono esperienze significative sulla loro pelle: momenti talmente importanti da generare in loro un cambiamento.
Questi momenti di crescita si chiamano \glo{prove cruciali}.

I \glo{giocatori} sono liberi di decidere quali sono le \glo{prove cruciali} per i loro \glo{eroi}, ma dovranno farlo prima di conoscerne l'esito.
Solo dopo averle affrontate potranno raccontare come queste prove hanno cambiato gli \glo{eroi}.

Non è sulla base della convenienza che il \glo{giocatore} fa evolvere il suo \glo{eroe}, ma sulla base della sua psicologia e del significato che l'\glo{eroe} trova nelle esperienze che vive.

Per far evolvere l'eroe devi seguire questi passaggi.
\begin{enumerate}
    \item \emph{Dichiara che la prova è cruciale per l'eroe.}
        Puoi farlo in qualsiasi momento desideri e ogni volta che lo ritieni opportuno, ma prima di aver \glo{estratto} i \tokenrand{}.
    \item \emph{Affronta la prova.}
        Affronta la \glo{prova} come tutte le altre, seguendo le regole del \namecref{chap:affrontare-prove} \nameref{chap:affrontare-prove}.
    \item \emph{Descrivi come l'esito della prova cambia l'eroe.}
        Dopo aver affrontato la \glo{prova cruciale} puoi far evolvere l'\glo{eroe} in uno di questi modi:
        \begin{itemize}
            \item aggiungi un \glo{tratto};
            \item cambia un \glo{tratto} già esistente con un altro;
            \item segnalo con una \glo{cicatrice};
            \item fagli apprendere una \glo{lezione}.
        \end{itemize}
\end{enumerate}

\section{Dichiarare una \glo{prova cruciale}}
Puoi
\marginpar{Per Neo, in \foreignlanguage{english}{\emph{The Matrix}}, il momento in cui decide di affrontare l'agente Smith invece di fuggire è un momento di svolta, una \glo{prova cruciale}.
    Allo stesso modo, la scelta di Batman di salvare Harvey Dent invece di Rachel è ciò che ha trasformato Harvey in Due Facce.}
dichiarare che qualsiasi \glo{prova} è \glo{cruciale} per lo sviluppo del tuo \glo{eroe}, ma solo prima che sia stata effettuata l'\glo{estrazione}.
Puoi dichiarare \glo{cruciali} \glo{prove} tue o di altri \glo{giocatori}.

In altre parole, puoi decidere liberamente quando il tuo \glo{eroe} cambierà, ma devi farlo prima di sapere quale sarà l'esito della \glo{prova}, e quindi come il tuo \glo{eroe} ne verrà influenzato.

\subsection{Come distinguere una \glo{prova cruciale}}
In
\marginpar{Fabio sa già che per Etienne il momento in cui si scontrerà con gli assassini di sua moglie sarà una \glo{prova cruciale}.
    Tuttavia Fabio potrebbe rimanere stupito nel rendersi conto che per Etienne aiutare un bambino a trovare la strada di casa significa talmente tanto da essere una \glo{prova cruciale} quanto lo scontro con le sue nemesi.}
ogni \glo{storia} gli \glo{eroi} si trovano ad affrontare momenti che significano molto per loro e in cui la posta in gioco è molto alta.
Le \glo{prove cruciali} sono tanto importanti da condizionare non solo il corso della \glo{storia}, ma anche lo sviluppo dell'\glo{eroe}.

Quando non starai nella pelle nell'attesa di scoprire cosa sta per accadere e ti chiederai come potrebbe segnare il tuo \glo{eroe}, allora saprai di stare affrontando una \glo{prova cruciale}.
Se non ti senti coinvolto a sufficienza nella \glo{prova} che sta per essere affrontata, evidentemente quella non è una \glo{prova cruciale}.

\subsection{Quanto spesso posso dichiarare \glo{prove cruciali}}
Non esistono regole sulla frequenza delle \glo{prova cruciale}, sebbene dichiarare un massimo di una \glo{prova cruciale} a \glo{sessione} sia ottimale per uno sviluppo equilibrato dell'\glo{eroe}.

Le \glo{prove cruciali} dipendono dal corso della \glo{storia} e dalle esperienze vissute dagli \glo{eroi}.
Non strumentalizzare le \glo{prove} per velocizzare la crescita dell'\glo{eroe}.
In \nte{} crescere non è uno scopo né un premio, ma un modo per vivere pienamente la \glo{storia}.

\section{Affrontare una \glo{prova cruciale}}
Una
\marginpar{Lilian assiste al tentativo di Etienne di giustiziare uno degli assassini di sua moglie.
    Alice dichiara che la \glo{prova} di Etienne è \glo{cruciale} per Lilian, perché gli ideali di Lilian si contrappongono all'odio che prova per gli assassini della moglie di Etienne.
    Alice attende che Etienne abbia affrontato la \glo{prova} per raccontare come il suo esito cambierà Lilian.}
\glo{prova cruciale} va affrontata nello stesso modo di una qualsiasi altra \glo{prova}.
Per affrontare una \glo{prova cruciale}, segui le indicazioni che trovi nel \namecref{chap:affrontare-prove} \nameref{chap:affrontare-prove}.

Se hai dichiarato \glo{cruciale} la \glo{prova} di un altro \glo{eroe}, lascia che l'altro \glo{giocatore} la affronti, potrai decidere e narrare il modo in cui l'esito della \glo{prova} cambia il tuo \glo{eroe} dopo che l'esito della \glo{prova} sarà stato narrato.

\subsection{Se la \glo{prova} si affronta normalmente, cosa cambia rispetto al solito?}
Lo spirito con cui la si affronta e la trepidazione nel conoscerne l'esito.
Affrontare una \glo{prova} sapendo che cambierà il tuo \glo{eroe} aggiunge all'importanza dell'azione anche il coinvolgimento personale.
Le regole utilizzate per affrontare la \glo{prova} includono già tutto ciò che serve per permettere il giusto livello di coinvolgimento, sapere che la \glo{prova} influenzerà l'evoluzione dell'\glo{eroe} farà tutta la differenza.

\subsection{Cosa succede se fallisco o \glo{esco di scena} durante una \glo{prova cruciale}?}
In
\marginpar{Affrontare una \glo{prova cruciale} in cui cerchi di convincere una folla a sollevarsi contro la tirannia potrebbe renderti \emph{Idealista } o \emph{Disilluso} a seconda del suo esito, ma entrambi i \glo{tratti} potranno essere messi in gioco durante le \glo{prove} che affronterai successivamente nella \glo{storia}.}
termini di regole, niente di diverso rispetto a quello che succederebbe se avessi successo.
Dato che i \glo{tratti} possono essere messi in gioco anche quando sono parole con un significato negativo, qualunque sia il modo in cui il tuo \glo{eroe} evolverà porterà comunque dei vantaggi.
A cambiare sarà il modo in cui lo giocherai e l'idea che hai di lui.
La meccanica a supporto del modo in cui lo interpreti rimarrà la stessa.

\section{Far evolvere l'\glo{eroe} dopo la \glo{prova}}
In base all'esito della \glo{prova cruciale}, fai evolvere l'\glo{eroe} in uno di questi modi:
\begin{itemize}
    \item aggiungi un \glo{tratto};
    \item cambia un \glo{tratto} già presente con un altro;
    \item segnalo con una \glo{cicatrice};
    \item fagli apprendere una \glo{lezione}.
\end{itemize}

\subsection{Come decidere il modo in cui la \glo{prova cruciale} cambia l'\glo{eroe}}
Non ci sono regole né vincoli su quale opzione scegliere.
Le prossime pagine ti forniranno caso per caso suggerimenti e linee guida utili a scegliere quale opzione prediligere in base all'esito della \glo{prova}.

Quando scegli l'impatto che l'esito di una \glo{prova cruciale} ha sull'\glo{eroe}, tieni presente il contesto in cui è avvenuta, come il tuo \glo{eroe} ha agito per affrontarla, quali erano gli elementi in gioco e ovviamente quanti \tokensucc{} e \tokenfail{} sono stati \glo{estratti}.

\subsection{Quando è giusto far evolvere l'\glo{eroe}}
Gli
\marginpar{William Wallace avrebbe preferito non assistrere all'esecuzione sommaria di sua moglie, ma purtroppo per lui questa cosa è successa e lo ha cambiato per sempre, segnando l'inizio di un percorso costellato di \glo{prove} e momenti decisivi che lo hanno trasformato in un'icona della rivoluzione.}
\glo{eroi} di \nte{} non evolvono in base a quanto è conveniente sviluppare il personaggio, ma in base alle esperienze significative che vivono sulla loro pelle e che li portano a cambiare.

Quando fai evolvere l'\glo{eroe} immergiti nella sua psicologia, chiediti come l'esperienza che ha vissuto abbia influito su di lui, consulta la scheda, focalizza quali sono gli aspetti chiave che sono stati influenzati dalla situazione.
Dopodiché, descrivi in modo vivido l'esito della \glo{prova cruciale}.

\section{Acquisire un nuovo \glo{tratto}}
Per acquisire un nuovo \glo{tratto}, scrivine uno a tua scelta in uno degli esagoni liberi dell'alveare, seguendo le regole del \namecref{chap:creare-eroe} \nameref{chap:creare-eroe}.

L'\glo{eroe}
\marginpar{Etienne ha accompagnato il ragazzino sano e salvo a casa.
    Erano settimane che pensava solo a vendicarsi, uccidere, distruggere.
    La fiducia incondizionata di qualcuno che gli ha chiesto di proteggerlo ha risvegliato qualcosa di colui che era prima della tragedia.
    Etienne aggiunge il \glo{tratto} \emph{Gentile}.}
potrebbe aver acquisito un \glo{tratto} se dopo aver affrontato una \glo{prova cruciale}:
\begin{itemize}
    \item ha imparato a fare qualcosa di nuovo;
    \item ha appreso qualcosa su sé stesso o sull'ambientazione;
    \item ha ampliato i propri orizzonti o maturato nuove convinzioni;
    \item ha stretto un legame importante con qualcuno o qualcosa;
    \item ha messo in luce un lato di sé stesso che fino a quel momento era rimasto nascosto.
\end{itemize}

\section{Cambiare un \glo{tratto}}
Per cambiare un \glo{tratto} cancella il contenuto di uno degli esagoni già utilizzati e cambialo con il \glo{tratto} più adatto all'\glo{eroe}.

L'\glo{eroe}
\marginpar{Lilian assiste a una \glo{prova} in cui Etienne racconta dell'assassinio di sua moglie per ottenere un'autorizzazione a cacciare legalmente i suoi assassini.
    Capire che Etienne vuole consumare la sua vendetta senza spargere sangue inutile è \glo{cruciale} per Lilian, che dopo la \glo{prova} cambia il suo \glo{tratto} \emph{Porgi l'altra guancia} con il \glo{tratto} \emph{Aiuta le vittime, combatti i carnefici}.}
potrebbe cambiare uno dei suoi \glo{tratti}, sia esso l'archetipo, una qualità, un'abilità o un tratto \glo{alternativo}, se dopo aver affrontato una \glo{prova cruciale}:
\begin{itemize}
    \item ha cambiato opinione su sé stesso o su di un elemento della \glo{storia};
    \item ha visto sconfessato qualcosa in cui credeva;
    \item ha visto  cambiare un legame importante con qualcuno o qualcosa;
    \item ha vissuto il termine naturale di un rapporto, un giuramento o qualcosa di importante;
    \item una condizione che rendeva un \glo{tratto} adeguato a descriverlo è cambiata radicalmente.
\end{itemize}

\section{Farsi segnare da una \glo{cicatrice}}
\label{sec:farsi-segnare-cicatrice}
Per
\marginpar{Lothar è sulle tracce di un criminale noto per la sua efferatezza.
    Le sue indagini lo conducono a una scena del crimine particolarmente cruenta, la cui vittima è una persona che Lothar conosceva bene.

    Roberto dichiara \glo{cruciale} la \glo{prova} in cui investiga la scena del crimine, Lothar non è disposto a farsi sfuggire neanche un dettaglio.
    Roberto \glo{estrae} \tokensucc{4}, un successo straordinario, ma decide che proprio questo successo ha scosso profondamente Lothar.

    Da dove gli viene questo intuito?
    Quando ha imparato a ragionare come un criminale?

    Roberto segna la \glo{cicatrice} \emph{Sto diventando come loro}.}
segnare l'\glo{eroe} con una \glo{cicatrice}, scegli un esagono libero e scrivi al suo interno una parola chiave, dopodiché sottolineala per distinguerla visivamente dai \glo{tratti}.

\subsection{Cosa sono le \glo{cicatrici}}
Le \glo{cicatrici} sono esperienze che hanno lasciato un segno sull'\glo{eroe}, da un lato esponendo una sua fragilità, ma dall'altro rendendolo più coriaceo e capace di tollerare le \glo{complicazioni}.

Proprio come un \glo{tratto}, una \glo{cicatrice} è una parola chiave che posizioni in uno degli esagoni liberi dell'alveare, ma a differenza di un \glo{tratto} va evidenziata o sottolineata, in modo da poter essere riconosciuta a colpo d'occhio.

\subsection{A cosa servono le \glo{cicatrici}}
\marginpar{Tempo dopo, Lothar si trova di fronte al criminale che stava cercando, che sta minacciando di tagliare la gola a un ostaggio.
    Lothar cerca di convincerlo a desistere e lasciare andare l'ostaggio.

    Roberto decide di mettere in gioco la \glo{cicatrice} di Lothar e aggiunge \tokenfail{1} oltre alla \glo{difficoltà} base.

    Lothar \glo{estrae} \tokensucc{1} e \tokenfail{1}.
    L'ostaggio viene rilasciato senza conseguenze, perché Roberto posiziona il \tokenfail{} sulla sua \glo{cicatrice}.
    Il criminale ha visto negli occhi di Lothar una luce simile alla sua e Lothar, in quegli occhi, ha riconosciuto lo stesso.
    }
Quando metti in gioco una cicatrice, aggiungi un \tokenfail{} al \glo{sacchetto}, ma potrai ignorare un \tokenfail{} che \glo{estrarrai} posizionandolo sulla \glo{cicatrice}, dove rimarrà fino a quando sarà adeguato per la narrazione.
Fino al momento in cui la \glo{cicatrice} sarà occupata da un \tokenfail{} non potrà essere messa in gioco di nuovo.

\subsection{Quando una \glo{cicatrice} è preferibile a un \glo{tratto}}
Segna il tuo \glo{eroe} con una \glo{cicatrice} quando affronta la \glo{fine} di qualcosa importante per lui in modo traumatico.
La vita di un amico o  un avversario, un giuramento, un ideale, un lungo viaggio, l'idea che ha di qualcosa.

\subsection{Quindi le \glo{cicatrici} sono termini negativi e i \glo{tratti} termini positivi?}
Assolutamente
\marginpar{Per William Wallace essere un \emph{Eroe del popolo} potrebbe essesre una \glo{cicatrice} più che un \glo{tratto}.
    Qualcosa che è diventato suo malgrado e che lo fa vivere isolato da tutto e da tutti, solo per tutta la sua vita.
    Inoltre lo è diventato a causa di un evento traumatico: la morte di sua moglie.
    Al contrario, per Zat\=oichi essere \glo{Cieco} è un vantaggio per poter focalizzare i suoi sensi e le sue abilità di spadaccino.
    Probabilmente Zat\=oichi porterebbe la sua cecità in gioco più come un \glo{tratto} che come una \glo{cicatrice}.}
no.
Pensare che i \glo{tratti}, aggiungendo \tokensucc{} al \glo{sacchetto}, debbano essere termini positivi, mentre le \glo{cicatrici}, aggiungedo \tokenfail{} al \glo{sacchetto}, debbano essere termini negativi, è un errore piuttosto comune.

Alcuni \glo{eroi} famosi vivono come \glo{cicatrici} aspetti della loro vita che altre persone potrebbero considerare come grandi fortune, mentre molti \glo{eroi} mettono a frutto aspetti traumatici e negativi della loro vita, trasformandoli nei loro più grandi punti di forza.

La differenza tra un \glo{tratto} e una \glo{cicatrice} non dipende da come è scritta o formulata, ma da come l'\glo{eroe} la vive.
Un \glo{tratto} è una caratteristica che metti in gioco a tuo vantaggio, una \glo{cicatrice} è una fragilità che ti permette di essere più resistente alle \glo{complicazioni}.

\subsection{Quante \glo{cicatrici} possono segnare un \glo{eroe}?}
Non c'è un limite al numero di \glo{cicatrici} da cui puoi essere segnato, considera tuttavia che le \glo{cicatrici} sono veri e propri traumi indelebili impressi nella psicologia del tuo \glo{eroe}.

Un \glo{eroe} da più di 2 \glo{cicatrici} dobrebbe essere interpretato come qualcuno che è stato segnato da esperienze davvero forti e ne avverte il peso sulle spalle.

Nella \namecref{part:affrontare-fine} dedicata ad affrontare la \glo{fine dell'eroe}, scoprirai che il numero delle \glo{cicatrici} è una delle linee guida più importanti da tenere in considerazione quando si tratta di scegliere di mettere o meno la parola \glo{fine} alla \glo{storia} dell'\glo{eroe}.

\section{Imparare una lezione}
Per imparare una \glo{lezione}, scegli una carta \glo{lezione} e aggiungila alla scheda dell'\glo{eroe}.

\subsection{Cosa sono le lezioni?}
Le \glo{lezioni} sono capacità che ti permettono di utilizzare i \tokenrand{} che \glo{estrai} in modo unico.
Ogni \glo{lezione} è descritta in una carta che puoi mettere vicino alla scheda durante le \glo{sessioni} o scrivere nella seconda pagina della scheda dell'\glo{eroe}.
Puoi apprendere una \glo{lezione} dopo una \glo{prova cruciale} il cui esito sia narrativamente adatto a insegnartela.

\subsection{Come scelgo la lezione?}
Ogni
\marginpar{Lilian si è lanciata all'inseguimento di uno degli assassini della moglie di Etienne, cosa che lo ha molto colpito, dimostrandogli quanto lei tenga a lui.
    Lilian non ha avuto successo ed è rimasta sfregiata.
    Etienne promette a Lilian che la farà pagare al fuggiasco e Fabio decide che Etienne impara la \glo{lezione} \emph{Nessuno fugge per sempre}.}
\glo{lezione} che impari dovrebbe essere legata alla \glo{prova cruciale} che hai vissuto.
Non sei obbligato a scegliere una specifica \glo{lezione} piuttosto che un'altra, ma ricorda che il modo in cui l'\glo{eroe} evolve dovrebbe essere legato a doppio filo alle esperienze che vive.

\subsection{Quante lezioni posso imparare?}
Non c'è un limite al numero di \glo{lezioni} che puoi imparare, tuttavia considera che le \glo{lezioni} aggiungono complessità al tuo \glo{eroe}.
Se ami strutturare il tuo \glo{eroe} oltre il regolamento di base, espandine le potenzialità tramite le \glo{lezioni}, se preferisci personaggi semplici e immediati, evita le \glo{lezioni} e concentrati sui \glo{tratti}.
Di norma un \glo{eroe} bilanciato in termini di complessità non conosce più di 2 o 3 \glo{lezioni}.

\subsection{Lezioni}
\subsubsection{Lezioni per onore, giustizia, purezza, disciplina}
\lesdesc{\input{lezioni/onore-sacrificio}}
    {
        Il tuo alleato non è obbligato a consegnarti il \tokenfail{}.
        Puoi usare questa \glo{lezione} prima che effetti del \glo{pericolo} entrino in gioco.
        Puoi decidere di aggravare questa \glo{sventura} facendoti carico di altri \tokenfail{} se la situazione in gioco lo giustifica.
    }

\lesdesc{\input{lezioni/colpa-ombra}}
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/disciplina-ferrea}}
    {
        Gli effetti del \glo{pericolo} si applicano prima che tu possa usare questa \glo{lezione}.
    }

\subsubsection{Lezioni per pace, amore, fratellanza, perdono}
\lesdesc{\input{lezioni/aiuta-prossimo}}%
    {
        Se la \glo{prova} in cui aiuti ha un \glo{pericolo}, il fatto che si applichi o meno al tuo \glo{eroe} dipende dal tuo giudizio e da come narri l'aiuto che dai quando usi questa \glo{lezione}.
    }

\lesdesc{\input{lezioni/momenti-bui-speranza}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/fiducia-incondizionata}}%
    {
        Puoi utilizzare questa \glo{lezione} in qualsiasi momento in cui lo ritieni opportuno.
    }

\subsubsection{Lezioni per ribellione, rivoluzione, libertà}
\lesdesc{\input{lezioni/nessun-ribelle-solo}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
        Collabora con il \glo{narratore} per raccontare cosa succede.
    }

\lesdesc{\input{lezioni/idee-no-padroni}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
        Collabora con il \glo{narratore} per raccontare cosa succede.
    }

\lesdesc{\input{lezioni/leggenda-vivente}}
    {
        Puoi decidere ad ogni \glo{prova} se usare la \glo{cicatrice} come \glo{cicatrice} o come \glo{tratto}, ma non potrai avere entrambi gli effetti contemporaneamente.
    }

\subsubsection{Lezioni per opportunità, furbizia, spirito critico}
\lesdesc{\input{lezioni/non-convincerli-confondili}}%
    {
        Spendere il \tokensucc{} su questa \glo{lezione} non occupa il  tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/occasione-uomo-ladro}}%
    {
        Spendere il \tokensucc{} su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/specializzarsi-insetti}}%
    {
        Cambia l'abilità su questa \glo{lezione} all'inizio di ogni \glo{sessione}, non potrai farlo durante il gioco appena prima di una \glo{prova} che stai per affrontare.
    }

\subsubsection{Lezioni per ragione, verità, conoscenza}
\lesdesc{\input{lezioni/diavolo-dettagli}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/esperimento-fallito-successo}}%
    {
        Gli effetti del \glo{pericolo} si applicano dopo aver usato questa \glo{lezione}.
    }

\lesdesc{\input{lezioni/mente-sopra-corpo}}%
    {
        Puoi usare questa \glo{lezione} ogni volta che sei \glo{confuso}.
    }

\subsubsection{Lezioni per vendetta, riscatto, furia}
\lesdesc{\input{lezioni/sangue-chiama-sangue}}%
    {
        Gli effetti del \glo{pericolo} si applicano prima che tu possa usare questa \glo{lezione}.
        In ogni caso, se lo desideri puoi segnare la \glo{sventura} che ti fa \glo{uscire di scena} su questa \glo{lezione}.
    }

\lesdesc{\input{lezioni/nessuno-fugge-sempre}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/subirete-ira}}%
    {
        Gli effetti del \glo{pericolo} si applicano prima che tu possa usare questa \glo{lezione}.
    }

\subsubsection{Lezioni per grinta, coraggio, tenacia}
\lesdesc{\input{lezioni/risica-rosica}}%
    {
        Gli effetti del \glo{pericolo} si applicano prima che tu possa usare questa \glo{lezione}.
    }

\lesdesc{\input{lezioni/saper-osare}}%
    {
        Puoi usare questa \glo{lezione} ogni volta che affronti una \glo{prova}.
    }

\lesdesc{\input{lezioni/ultima-risorsa}}%
    {
        Puoi usare questa \glo{lezione} anche quando aiuti qualcuno, ma devi saper descrivere come lo fai in maniera convincente.
    }

\subsubsection{Lezioni per fede, certezza, fiducia}
\lesdesc{\input{lezioni/conosco-destino}}%
    {
        Gli effetti del \glo{pericolo} si applicano prima che tu possa usare questa \glo{lezione}.
        Se lo desideri, puoi \glo{rischiare} dopo aver \glo{estratto}.
    }

\lesdesc{\input{lezioni/corpo-sanguina-idea-immortale}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/abbraccia-fato}}%
    {
        Questa \glo{lezione} ha effetto prima che gli effetto del \glo{pericolo} entrino in gioco.
    }

\subsubsection{Lezioni per arte, espressione di sé}
\lesdesc{\input{lezioni/nessuno-indifferente-bellezza}}%
    {
        Spendere il \tokensucc{} posizionato su questa \glo{lezione} non occupa il tuo \glo{turno}, puoi farlo in qualsiasi momento.
    }

\lesdesc{\input{lezioni/assonanza}}%
    {
        Questa \glo{lezione} è sempre attiva, non puoi decidere di non utilizzarla.
        Quando fai evolvere l'\glo{eroe} puoi cambiare un \glo{tratto} in assonanza, ma non eliminarlo.
    }

\lesdesc{\input{lezioni/dissonanza}}%
    {
        Questa \glo{lezione} ha effetto prima che gli effetti del \glo{pericolo} entrino in gioco.
        Questa \glo{lezione} è sempre attiva, non puoi decidere di non utilizzarla.
        Quando fai evolvere l'\glo{eroe} puoi cambiare un \glo{tratto} in dissonanza, ma non eliminarlo.
    }
