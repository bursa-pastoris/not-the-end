## Tratti

In teoria le griglie esagonali dei Tratti potrebbero essere ridisegnate
direttamente nel codice sorgente di LaTeX con
[Ti*k*Z](https://www.ctan.org/pkg/pgf), ma questo richiederebbe un ingente
investimento di risorse. Per questo ho preferito utilizzare
[Inkscape](https://inkscape.org/).

Ogni griglia specifica è costruita partendo dal "modello" vuoto
[`img/tratti-generica.svg`](./img/tratti-generica.svg), del quale si crea una
copia in `./img`. I Tratti vanno inseriti:

* nel layer `tratti`;
* sotto forma di un oggetto di testo per ciascun Tratto;
* in font `FreeSerif` a dimensione 8, centrato orizzontalmente, con
  interlinea 1.

L'allineamento centrato verticalmente e orizzontalmente nell'esagono di
appartenenza si può ottenere facilmente con lo strumento `Object > Align and
Distribute... > Align`, impostando `Relative to:` su `First selected`.

Per inserire Tratti particolarmente lunghi senza esondare dall'esagono si
applicano, nell'ordine, le seguenti soluzioni finché non si è riusciti a
ottenere quanto desiderato:

1. spezzare il testo su più righe;
2. violare l'allineamento verticale nell'esagono;
3. ridurre la dimensione del font;
4. utilizzare abbreviazioni.

Per fare in modo che sia facile orientarsi nel documento vettoriale anche a
distanza di tempo, gli oggetti di testo contenenti i Tratti devono avere nome
uguale al contenuto, eventualmente abbreviato omettendo i monosillabi, ed
essere disposti dall'alto verso il basso nell'ordine seguente:

* archetipo;
* qualità, partendo da quella appena sopra l'archetipo e procedendo in senso
  orario;
* abilità, partendo da quella sopra l'archetipo e procedendo in senso orario.

Prima di effettuare il commit bisogna bloccare tutti i layer, incluso `tratti`.

Inoltre, per mantenere più pulito il codice sorgente senza perdere la facilità
di modifica del documento con Inkscape, prima di fare il commit è opportuno
rimuovere manualmente dal documento SVG il tag `<sodipodi:namedview>` con tutto
il suo contenuto. Questo evita inoltre di registrare nel repository alcune
informazioni relative alle impostazioni di Inkscape.

## Sezionamento

Uno dei problemi del manuale originale è che il sezionamento è poco chiaro: ci
sono intestazioni che sembrano dello stesso livello che però hanno formato
diverso, altre che hanno formato simile sembrano essere di livello diverso.  I
relativi livelli sono stati ricostruiti "a occhio e croce".

## Lezioni

Dal manuale di gioco e dalla struttura della scheda del personaggio, sembra che
i rettangoli disegnati nel primo debbano essere utilizzati come carte, quindi
che debbano essere stampati e tagliati. Contemporaneamente però sembrerebbe
poco logico costringere i giocatori a tagliare le pagine del manuale o a farne
fotocopie, che sarebbero necessariamente poco precise.  Per risolvere il
dubbio, in questa versione si è deciso di riportare le "carte" delle lezioni
sia nel manuale, accoppiate alle rispettive descrizioni, sia in un documento a
parte, più adatto per la stampa.

Per assicurare che il contenuto delle carte nei due documenti sia esattamente
lo stesso, ciascuna di esse è generata da uno specifico documento sorgente,
incluso sia nel manuale che nella versione per la stampa.  Tali singoli
documenti sono nel percorso [`lezioni`](./lezioni).

Per far fronte alla necessità di costruire due documenti distinti (manuale di
gioco e carte da stampare) contenenti gli stessi elementi disposti diversamente
nello spazio, l'elemento presente nel manuale originale è stato "scomposto" in
tre parti:

    MANUALE ORIGINALE                    ||    PARTI
                                         ||
                                         ||
      +---------------------------+      ||      +---------------------------+
      | L'onore è sacrificio      |      ||      |             1             |
      +---------------------------+      ||      +---------------------------+
      | Quando un alleato ESTRAE  |      ||      |                           |
      | 1 o più * puoi chiedergli |      ||      |                           |
      | di collocarne uno qui.    |      ||      |                           |
      |                           |      ||      |             2             |
      | Descrivi come accorri in  |      ||      |                           |
      | suo aiuto e fatti carico  |      ||      |                           |
      | di una SVENTURA per       |      ||      |                           |
      | proteggerlo dalle         |      ||      |                           |
      | avversità.                |      ||      |                           |
      |                           |      ||      |                           |
      +---------------------------+      ||      +---------------------------+
                                         ||
     Il tuo alleato non è obbligato      ||
     a consegnarti il *. Puoi usare      ||
     questa lezione prima che            ||
     effetti del PERICOLO entrino        ||
     in gioco. Puoi decidere di          ||                   3
     aggravare questa SVENTURA           ||
     facendoti carico di altri * se      ||
     la situazione in gioco lo           ||
     giustifica.                         ||
                                         ||
                                         ||
                                         ||

1. Titolo
2. Effetto
3. Descrizione

Sia il manuale che la versione per la stampa contengono la carta (titolo,
effetto e contorni), mentre la descrizione è presente solo nel primo.  I
singoli documenti per lezione contengono quindi le istruzioni per disegnare la
carta, attraverso il comando `\les`:

    % lezioni/esempio.tex
    \les{Titolo}%
        {
            Effetto.

            Eventualmente in più paragrafi.
        }

Tale documento è poi usato nel manuale per associare la carta alla rispettia
descrizione con il comando `\lesdesc`:

    \lesdesc{\input{lezioni/esempio}}
        {
            Descrizione della lezione.

            Eventualmente su più paragrafi.
        }

La versione stampabile è stata costruita disponendo sul foglio A4 le sole
carte, con

    \input{lezioni/esempio}

`\les` e `\lesdesc` sono definiti in [`lezioni.tex`](./lezioni.tex).
