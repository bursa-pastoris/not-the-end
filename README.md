Questo progetto nasce con l'obiettivo di risolvere alcuni problemi tipografici
del manuale del gioco di ruolo *Not The End*, soprattutto l'impaginazione su
fogli quadrati e l'uso poco efficiente dello spazio, che danno problemi nel
caso si voglia stampare il documento per averlo sempre a portata di mano
durante il gioco pur senza acquistarlo.  Questo obiettivo è raggiunto
riscrivendo l'intero manuale in LaTeX, approfittando del fatto che sia
distribuito sotto licenza CC-BY-NC-SA 3.0 IT.

Il manuale originale in Italiano si può acquistare sul [sito di
Fumble](https://fumblegdr.it), a [questa
pagina](hhttps://www.fumblegdr.it/products/not-the-end-manuale-base), o
scaricare gratuitamente dalla rete eD2K
(`ed2k://|file|not_the_end.pdf|10553973|A7C10460150E36D01B7C5CA65D43E4BD|/`)[^eD2K]
o alla pagina <https://cloud.disroot.org/s/ssFjA3wCMSBbo4w>.

In passato sul sito di Fumble GDR si trovavano anche materiali di gioco
aggiuntivi scaricabili liberamente, tuttora reperibili grazie a [Internet
Archive](https://archive.org/).  Sono disponibili anche su Disroot alla pagina
<https://cloud.disroot.org/s/T9qS3o6AFkdoZMe>.

## Diritto d'autore

> Questa sezione segue le definizioni stabilite dalla licenza CC-BY-NC-SA 3.0
> IT

La presente è un'Opera Derivata dall'Opera intitolata *Not The End*, creata
dagli Autori Originari seguenti.

* Idea e Art Direction: Claudio Pustorino
* Game Design: Claudi Pustorino, Fabio Airoldi, Claudio Serena
* Illustrazioni: Pietro Bastas
* Impaginazione e layout: Claudio Pustorino
* Editing: Carlo Serena

Le modifiche introdotte in questa Opera Derivata sono prevalentemente di natura
tipografica e secondariamente di formulazione del testo.
Sono stati inoltre rimossi tutti i disegni.

L'Opera originaria e questa Opera Derivata sono distribuite sotto licenza
CC-BY-NC-SA 3.0 IT, le cui condizioni sono riassunte alla pagina
<https://creativecommons.org/licenses/by-nc-sa/3.0/it/> e il cui testo
legalmente valido può essere consultato alla pagina
<https://creativecommons.org/licenses/by-nc-sa/3.0/it/legalcode>.

## Contributi

Chiunque lo desideri può contribuire al progetto tramite una pull-request al
[repository su Disroot](https://git.disroot.org/bursa-pastoris/not-the-end)
oppure inviando le proprie modifiche o aggiunte per posta elettronica a
`bursapastoris at disroot dot org` con le accortezze indicate
[qui](https://git.disroot.org/bursa-pastoris/info/src/branch/master/it/contact.md).

Nel copiare in LaTeX il contenuto del manuale originale, è bene attenersi alle
istruzioni contenute in [GUIDELINES.md](./GUIDELINES.md). Questo consente di
mantenere il codice sorgente uniforme e ordinato.

## Ringraziamenti

Grazie a Fumble per avere distribuito questo manuale sotto una licenza Creative
Commons!

> Non abbiamo paura del digitale, siamo tutti professionisti che col digitale
> ci campano, e sappiamo che chi cerca di scaricare i PDF di un manuale è
> perchè tanto non l’avrebbe comprato lo stesso fisico. Preferiamo a questo
> punto cercare un nuovo modo di farvi fruire i nostri giochi in maniera comoda
> per voi e remunerativa per noi
>
> *dal [manifesto di Fumble](https://www.fumblegdr.it/manifesto/)*



[^eD2K]: Per la connessione alla rete eD2K è necessario un apposito programma,
  ad esempio [aMule](http://amule.org/).

* * *

![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc-sa.svg)
